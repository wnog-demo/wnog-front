const withCSS = require('@zeit/next-css');

module.exports = withCSS({
  useFileSystemPublicRoutes: false,
  webpack: (config, { buildId, dev, isServer, defaultLoaders }) => {
    config.resolve.modules.push(__dirname);
    const originalEntry = config.entry;
    config.entry = async () => {
      const entries = await originalEntry();
      if (entries['main.js']) {
        entries['main.js'].unshift('./polyfills.js');
      }
      return entries;
    };
    return config;
  }
});
