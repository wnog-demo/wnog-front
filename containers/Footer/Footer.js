import React, { Component } from 'react';
import Block from './Footer.style';
import { Text, Icon } from 'elements';

export default class Footer extends React.PureComponent {
  render() {
    const { footerIsBlack } = this.props;

    const year = process.browser && new Date().getFullYear();

    return (
      <Block.Root footerIsBlack={footerIsBlack}>
        <Block.Wrapper>
          <Block.InnerWrapper>
            <Block.SupportMail as={'a'} href={'mailto:support@wnoglogistics.com'}>
              <Text view="opacityBold" as={'span'}>
                support@wnoglogistics.com
              </Text>
            </Block.SupportMail>
          </Block.InnerWrapper>
          <Block.InnerWrapper />
          <Block.InnerWrapper>
            <Block.Copyright>
              <Text view="note">{year} © WNOG Logistics</Text>
            </Block.Copyright>
            {/* <Block.Nimax>
              <Text view="bold">Разработали —</Text>
              <Block.NimaxLogo as={'a'} href={'https://nimax.ru/'} target="_blank">
                <Icon name="nimax" />
              </Block.NimaxLogo>
            </Block.Nimax> */}
          </Block.InnerWrapper>
        </Block.Wrapper>
      </Block.Root>
    );
  }
}
