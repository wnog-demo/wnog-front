import styled, { css } from 'styled-components';

const Footer = {};

Footer.Root = styled.footer`
  height: 70px;
  display: flex;
  align-items: center;
  margin-top: auto;
  ${({ footerIsBlack, theme: { color } }) => css`
    background-color: white;
    ${footerIsBlack &&
      css`
        background-color: #111428;
        color: white;
      `}
  `}
  @media screen and (max-width: 1024px) {
    height: 80px;
  }
  @media screen and (max-width: 600px) {
    height: auto;
    padding: 1.5rem 0;
  }
`;

Footer.Wrapper = styled.div`
  width: 100%;
  max-width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-wrap: nowrap;
  padding: 0 4.5rem;
  @media screen and (max-width: 1024px) {
    padding: 0 2.5rem;
  }
  @media screen and (max-width: 600px) {
    display: block;
  }
`;

Footer.InnerWrapper = styled.div`
  width: calc(100% / 3);
  display: flex;
  @media screen and (max-width: 600px) {
    width: auto;
    display: block;
    text-align: center;
    :not(:last-child) {
      margin-bottom: 1rem;
    }
  }

  :nth-child(2) {
    justify-content: center;
  }

  :last-child {
    justify-content: flex-end;
  }
`;

Footer.SupportMail = styled.button`
  span {
    transition: all 0.2s ease;
  }

  :hover,
  :focus {
    span {
      opacity: 0.8;
    }
  }
`;

Footer.Copyright = styled.div``;

Footer.Nimax = styled.div`
  display: flex;
  align-items: baseline;
  @media screen and (max-width: 600px) {
    padding-top: 1rem;
    display: inline-flex;
  }
`;

Footer.NimaxLogo = styled.div`
  padding-left: 3px;
  padding-bottom: 12px;
  font-size: 40px;
  line-height: 0.42em;
`;

export default Footer;
