import styled, { css } from 'styled-components';
import { STAGE_STATUSES } from 'lib/constants';

const StatusesTable = {};

StatusesTable.Root = styled.div`
  padding: 2rem 0;
  background-color: white;
  min-height: calc(100vh - 170px);

  ${({ detailIsActive }) => css`
    ${detailIsActive &&
      css`
        position: absolute;
        height: calc(100vh - 170px);
        overflow: hidden;
        top: 0;
      `}
  `}
`;

StatusesTable.Table = styled.div`
  width: 100%;
  text-align: left;
`;

StatusesTable.Thead = styled.div`
  display: flex;
  position: sticky;
  top: 100px;
  z-index: 2;
  transition: box-shadow 0.3s ease;
  background-color: white;
  body.scrolled-50px & {
    box-shadow: 0 20px 20px -15px rgba(0, 0, 0, 0.05);
  }
`;

StatusesTable.MainTd = css`
  padding: 40px 20px 40px 0;
  text-align: left;
  vertical-align: top;
  position: relative;
  cursor: pointer;
  :first-child {
    padding-left: 4.5rem;
  }
`;

StatusesTable.Td = styled.div`
  ${StatusesTable.MainTd}
  flex: 1;
  ${({ align }) => css`
    :first-child {
      width: 35%;
    }
    ${align &&
      css`
        text-align: ${align};
      `}
  `}
`;

StatusesTable.Th = styled(StatusesTable.Td)`
  ${StatusesTable.MainTd}
  font-size: 9px;
  letter-spacing: 0.11em;
  text-transform: uppercase;
  color: rgba(0, 0, 0, 0.3);
  opacity: 1;
  font-weight: 600;
  border-bottom: 2px solid white;
  z-index: 3;
  padding-top: 20px !important;
  padding-bottom: 15px !important;
`;

StatusesTable.Tr = styled.div`
  display: flex;
  border-top: 1px solid #f3f4f5;
  transition: background-color 0.2s ease, color 0.2s ease;
  ${({ theme: { color } }) => css`
    &:hover {
      background-color: ${color.darkBlue};
      color: white;
    }
  `}
`;

StatusesTable.Tbody = styled.div``;

StatusesTable.Title = styled.div`
  font-size: 12px;
  font-weight: bold;
  text-align: left;
`;

StatusesTable.Value = styled.div`
  font-size: 12px;
  opacity: 0.5;
  padding-top: 8px;
`;

StatusesTable.Flex = styled.div`
  display: flex;
  align-items: center;
`;

StatusesTable.Date = styled.div`
  background-color: white;
  font-weight: 600;
  font-size: 9px;
  display: flex;
  padding: 1px 5px;
  border-radius: 10px;
  margin-left: 15px;
  letter-spacing: 0.1em;
  transition: background-color 0.2s ease;
  span {
    transition: opacity 0.2s ease;
    opacity: 0.5;
  }
  ${({ theme: { color } }) => css`
    ${StatusesTable.Tr}:hover & {
      background-color: rgba(255, 255, 255, 0.2);
      span {
        opacity: 0.8;
      }
    }
  `}
`;

StatusesTable.Status = styled.div`
  display: flex;
  align-items: center;
  flex-shrink: 0;
  position: absolute;
  left: -80px;
  margin-top: 4px;
`;

StatusesTable.StatusWrapper = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  color: black;
`;

StatusesTable.Reference = styled.div`
  display: flex;
  padding-right: 4rem;
`;

StatusesTable.ReferenceUser = styled.div`
  text-align: left;
`;

StatusesTable.Link = styled.div`
  display: flex;
  flex: 1;
`;

const colors = {
  [STAGE_STATUSES.FAILURE]: 'red',
  [STAGE_STATUSES.SUCCESS]: 'green',
  [STAGE_STATUSES.PROCESS]: 'yellow'
};

StatusesTable.StatusStep = styled.div`
  height: 7px;
  width: 7px;
  background-color: #d6d6de;
  border-radius: 50%;
  margin-left: 10px;
  position: relative;
  ${({ active, status, theme: { color: gColor } }) => css`
    ${status &&
      css`
        background-color: ${gColor[colors[status]]};
      `}
    ${active &&
      css`
        height: 10px;
        width: 10px;
      `}
  `}
`;

export default StatusesTable;
