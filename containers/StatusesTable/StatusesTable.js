import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import { graphql, compose, withApollo } from 'react-apollo';
import PropTypes from 'prop-types';
import moment from 'moment';

import { StatusHistory } from 'components';
import { Tooltip, Hint } from 'elements';
import { Link, Router } from 'lib/routes';
import { i18n } from 'lib/constants';
import { getLastStages, getUserData } from 'graphql/selectors';
import { STAGE_NAMES, TRANSPORTATION_STATUSES, USER_ROLES } from 'lib/constants';
import { GET_USER } from 'graphql/query';
import { formatTime } from 'lib/utils';
import Block from './StatusesTable.style';

const mapStateToProps = state => {
  return {
    app: state.app
  };
};

@compose(graphql(GET_USER, { name: 'userData' }))
@withRouter
@connect(
  mapStateToProps,
  null
)
export default class StatusesTable extends React.PureComponent {
  linkRefs = {};

  static propTypes = {
    data: PropTypes.array,
    onLinkClick: PropTypes.func,
    getRowRef: PropTypes.func
  };

  goPage(event, uid) {
    const { onLinkClick } = this.props;
    const node = this.linkRefs[uid];
    onLinkClick(event, uid, node);
  }

  linkRefHandler = node => {
    if (!node) return;
    const uid = node.dataset.uid;
    this.linkRefs[uid] = node;
  };

  render() {
    const {
      app: { lang },
      data,
      userData,
      getRowRef,
      router
    } = this.props;

    const userInfo = getUserData(userData.user);
    const isManager = userInfo.role === USER_ROLES.MANAGER;
    const detailIsActive = !!router.query.uid;

    return (
      <Block.Root detailIsActive={detailIsActive}>
        <Block.Table>
          <Block.Thead>
            <Block.Th>{i18n('customs_clearance', lang)}</Block.Th>
            <Block.Th>{i18n('status_and_date', lang)}</Block.Th>
            <Block.Th>{i18n('customs_and_coordinator', lang)}</Block.Th>
            <Block.Th>{i18n('transportation', lang)}</Block.Th>
          </Block.Thead>
          <Block.Tbody>
            {data.map(row => {
              const {
                uid,
                reference,
                stages,
                custom,
                contact,
                route,
                user,
                transportations,
                delivery
              } = row;

              const referenceUser = isManager ? (user ? user.name : '—') : false;

              const lastStages = getLastStages(stages);
              const lastStage = lastStages.length > 0 && lastStages[lastStages.length - 1];

              const lastStageDate =
                lastStage &&
                (lastStage.date
                  ? moment(formatTime(lastStage.date)).format('DD.MM.YY')
                  : undefined);
              const lastTransportation =
                transportations &&
                !!transportations.length &&
                transportations[transportations.length - 1];

              const deliveryIsShown =
                lastTransportation &&
                [TRANSPORTATION_STATUSES.SHIPPED, TRANSPORTATION_STATUSES.CONFIRMED].includes(
                  lastTransportation.slug
                ) &&
                delivery;

              return (
                <Block.Tr
                  key={uid}
                  onClick={e => this.goPage(e, uid)}
                  ref={node => getRowRef(uid, node)}
                >
                  <Block.Td align={'right'}>
                    <Block.Reference>
                      <Link route={'/clearance/' + uid} passHref>
                        <Block.Link data-uid={uid} ref={this.linkRefHandler} as={'a'}>
                          <Block.Title onClick={e => e.preventDefault()}>{reference}</Block.Title>
                        </Block.Link>
                      </Link>
                    </Block.Reference>
                    {referenceUser && (
                      <Block.ReferenceUser>
                        <Block.Value>{referenceUser}</Block.Value>
                      </Block.ReferenceUser>
                    )}
                  </Block.Td>
                  <Block.Td data-tooltip={''}>
                    <Block.Status>
                      <Block.StatusWrapper>
                        <React.Fragment>
                          {[1, 2, 3].map(i => {
                            const active = lastStages.length === i;
                            const stage = lastStages[i - 1];

                            return (
                              <Block.StatusStep
                                active={active}
                                status={stage && stage.status}
                                key={i}
                              />
                            );
                          })}
                          {lastStages && lastStages.length > 0 && (
                            <Tooltip>
                              <StatusHistory colors={[]} data={lastStages} />
                            </Tooltip>
                          )}
                        </React.Fragment>
                      </Block.StatusWrapper>
                    </Block.Status>
                    <Block.Flex>
                      <Block.Title>{lastStage ? i18n(lastStage.type, lang) : '—'}</Block.Title>
                      {lastStageDate && (
                        <Block.Date>
                          <span>{lastStageDate}</span>
                        </Block.Date>
                      )}
                    </Block.Flex>
                    {lastStage && <Block.Value>{lastStage.title}</Block.Value>}
                  </Block.Td>
                  <Block.Td>
                    <Block.Title>
                      {custom &&
                        (lang === 'ru' ? (
                          <Hint label={<>{custom.fullname}</>}>
                            {custom.name}
                            {custom.city && ', ' + custom.city}
                          </Hint>
                        ) : (
                          <Hint label={<>{custom.name}</>}>{custom.city}</Hint>
                        ))}
                    </Block.Title>
                    <Block.Value>{contact && contact.name}</Block.Value>
                  </Block.Td>
                  <Block.Td>
                    <Block.Title>{lastTransportation ? lastTransportation.title : '—'}</Block.Title>
                    {deliveryIsShown && <Block.Value>{delivery.name}</Block.Value>}
                  </Block.Td>
                </Block.Tr>
              );
            })}
          </Block.Tbody>
        </Block.Table>
      </Block.Root>
    );
  }
}
