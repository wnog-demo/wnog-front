import React, { Component } from 'react';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actionsCreators from 'store/actions';
import { graphql, compose, withApollo } from 'react-apollo';
import Block from './CreatePassword.style';
import { Text, Icon, TextInput, Button } from 'elements';
import { CHECK_SET_PASSWORD, SET_PASSWORD } from 'graphql/mutation';
import { signIn, setCookie, Validation } from 'lib/utils';
import { i18n } from '../../lib/constants';

const field = {
  value: '',
  isValid: false,
  showErrors: false,
  errorMessage: ''
};

const validationOptions = {
  password: {
    options: {
      required: true,
      mediumPassword: true
    }
  },
  repassword: {
    options: {
      required: true,
      mediumPassword: true
    }
  }
};

const mapStateToProps = state => {
  return {
    app: state.app,
    notification: state.notification
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      showNotification: bindActionCreators(actionsCreators.showNotification, dispatch),
      changeNotificationState: bindActionCreators(
        actionsCreators.changeNotificationState,
        dispatch
      ),
      changeToken: bindActionCreators(actionsCreators.changeToken, dispatch)
    }
  };
};

@withApollo
@withRouter
@connect(
  mapStateToProps,
  mapDispatchToProps
)
export default class CreatePassword extends React.PureComponent {
  isSent = false;
  reloadTimer = null;

  state = {
    isSent: false,
    hashIsChecked: false,
    hashIsValid: false,
    passwordsAreEqual: false,
    form: { showErrors: false },
    fields: {
      password: { ...field },
      repassword: { ...field }
    }
  };

  componentDidMount() {
    const { hash, email } = this.props.router.query;
    const {
      app: { lang }
    } = this.props;

    this.validation = new Validation({ config: validationOptions, lang });

    if (hash && email) {
      this.props.client
        .mutate({
          mutation: CHECK_SET_PASSWORD,
          variables: { hash, email }
        })
        .then(res => {
          console.log(res);
          if (res.data) {
            if (res.data.checkSetPassword) {
              this.setState({
                hashIsChecked: true,
                hashIsValid: true
              });
            } else {
              this.setState({
                hashIsChecked: true
              });
            }
          }
        });
    } else {
      this.setState({
        hashIsChecked: true
      });
    }
  }
  componentWillUnmount() {
    clearTimeout(this.reloadTimer);
  }

  get passwordsIsValid() {
    const { fields } = this.state;

    return !Object.keys(validationOptions).some(name => {
      return !fields[name].isValid;
    });
  }

  onInputChange(name, value) {
    const fields = this.validation.onInputChange(this.state.fields, name, value);

    this.setState({ fields }, e => {
      if (['password', 'repassword'].includes(name)) {
        const isEqual = this.state.fields.password.value === this.state.fields.repassword.value;
        if (isEqual !== this.state.passwordsAreEqual) {
          this.setState({
            passwordsAreEqual: isEqual
          });
        }
      }
    });
  }

  onInputFocus(name) {
    this.changeErrorVisibility(name, false);
  }

  onInputBlur(name) {
    this.changeErrorVisibility(name, true);
  }

  onAnyInputEvent(type, name, e) {
    if (type === 'change') name && this.onInputChange(name, e.target.value);
    if (type === 'focus') name && this.onInputFocus(name);
    if (type === 'blur') name && this.onInputBlur(name);
  }

  validate(name, value) {
    const valid = this.validation.validate(value, name);

    return valid;
  }

  onErrorMessageClose = name => {
    this.changeErrorVisibility(name, false);
  };

  changeErrorVisibility(name, bool) {
    const { fields } = this.state;
    const field = fields[name];

    this.setState({
      fields: {
        ...fields,
        [name]: {
          ...field,
          showErrors: bool
        }
      }
    });
  }

  isVisibleError(name) {
    const { fields, form } = this.state;
    const field = fields[name];

    return !field.isValid && (form.showErrors || field.showErrors);
  }

  onSubmit = e => {
    const {
      router,
      client,
      actions,
      app: { lang }
    } = this.props;
    const {
      fields: { password, repassword }
    } = this.state;

    e.preventDefault();

    if (this.isSent) return false;

    if (!(this.passwordsIsValid && this.state.passwordsAreEqual)) {
      this.onDisabledButtonClick();
      return false;
    }

    this.isSent = true;
    this.setState({
      isSent: true
    });

    client
      .mutate({
        mutation: SET_PASSWORD,
        variables: {
          hash: router.query.hash,
          email: router.query.email,
          password: password.value.trim(),
          repassword: repassword.value.trim()
        }
      })
      .then(res => {
        if (res.data && res.data.setPassword) {
          this.resetForm();
          const token = res.data.setPassword;
          setCookie('auth_token', token);
          if (router.route === '/restorePassword') {
            actions.showNotification(i18n('password_success_change', lang));
            this.reloadTimer = setTimeout(e => {
              signIn(token);
            }, 5000);
          } else {
            signIn(token);
          }
        }
      });
  };

  resetForm() {
    this.setState({
      fields: {
        password: { ...field },
        repassword: { ...field }
      }
    });
  }

  onDisabledButtonClick = e => {
    const {
      actions,
      app: { lang }
    } = this.props;
    const fields = this.validation.validateForm(this.state.fields);

    this.setState({
      fields,
      form: { showErrors: true }
    });

    if (!this.passwordsIsValid) {
      actions.showNotification(i18n('password_error', lang), 'error');
      return false;
    }
    if (!this.state.passwordsAreEqual) {
      actions.showNotification(i18n('passwords_not_match', lang), 'error');
      return false;
    }
  };

  render() {
    const {
      router,
      app: { lang }
    } = this.props;
    const {
      hashIsValid,
      hashIsChecked,
      fields: { password, repassword }
    } = this.state;
    const email = router.query.email;
    const isRestorePasswordRoute = router.route === '/restorePassword';
    const submitIsDisabled = !this.passwordsIsValid || !this.state.passwordsAreEqual;
    const title = isRestorePasswordRoute
      ? i18n('password_recovery', lang)
      : i18n('create_password', lang);

    return (
      <Block.Root>
        <Block.Wrapper>
          {!hashIsChecked && <div>{i18n('loading', lang)}...</div>}
          {hashIsChecked && !hashIsValid && (
            <React.Fragment>
              <div>{i18n('incorrect_link', lang)}</div>
            </React.Fragment>
          )}
          {hashIsValid && (
            <React.Fragment>
              <Block.Headline>
                <Text view={'h1'}>{title}</Text>
              </Block.Headline>
              {email && (
                <Block.LoginWrapper>
                  <Block.Login>
                    <Block.LoginIcon>
                      <Icon name="mail" />
                    </Block.LoginIcon>
                    <Text view="upperSpacing">{email}</Text>
                  </Block.Login>
                  <Block.LoginNote>
                    <Text view="note">— {i18n('your_login', lang)}</Text>
                  </Block.LoginNote>
                </Block.LoginWrapper>
              )}
              <Block.InputWrapper noValidate as={'form'} onSubmit={this.onSubmit}>
                <Block.InputItem>
                  <TextInput
                    name={'password'}
                    value={password.value}
                    invalid={this.isVisibleError('password')}
                    errorMessage={this.isVisibleError('password') && password.errorMessage}
                    onAnyEvent={this.onAnyInputEvent.bind(this)}
                    placeholder={i18n('password', lang)}
                    type={'password'}
                  />
                </Block.InputItem>
                <Block.InputItem>
                  <TextInput
                    name={'repassword'}
                    value={repassword.value}
                    invalid={this.isVisibleError('repassword')}
                    errorMessage={this.isVisibleError('repassword') && repassword.errorMessage}
                    onAnyEvent={this.onAnyInputEvent.bind(this)}
                    placeholder={i18n('repeat_password', lang)}
                    type={'password'}
                  />
                </Block.InputItem>
                <Block.ButtonItem>
                  {submitIsDisabled && (
                    <Block.DisabledButton onClick={this.onDisabledButtonClick} />
                  )}
                  <Button
                    type={'submit'}
                    onClick={this.onSubmit}
                    disabled={submitIsDisabled}
                    view="common"
                  >
                    {i18n('continue', lang)}
                  </Button>
                </Block.ButtonItem>
              </Block.InputWrapper>
            </React.Fragment>
          )}
        </Block.Wrapper>
      </Block.Root>
    );
  }
}
