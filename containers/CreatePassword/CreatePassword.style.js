import styled, { css } from 'styled-components';

const CreatePassword = {};

CreatePassword.Root = styled.div``;

CreatePassword.Wrapper = styled.div`
  padding-top: 6rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

CreatePassword.Headline = styled.div`
  text-align: center;
  max-width: 59%;
  margin-bottom: 3.375rem;
`;

CreatePassword.LoginWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  margin-bottom: 2.87rem;
`;

CreatePassword.LoginIcon = styled.div``;

CreatePassword.Login = styled.div`
  display: flex;
  align-items: center;
`;

CreatePassword.LoginNote = styled.div`
  margin-left: 4px;
`;

CreatePassword.LoginIcon = styled.div`
  font-size: 1em;
  line-height: 1;
  margin-right: 0.75rem;
  ${({ theme: { color } }) => css`
    color: ${color.blue};
  `}
`;

CreatePassword.InputWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 0 -10px 100px;
  align-items: flex-start;
`;

CreatePassword.InputItem = styled.div`
  margin: 0 10px;
  width: 260px;
  flex-shrink: 0;
`;

CreatePassword.ButtonItem = styled.div`
  display: flex;
  margin: 0 10px;
  flex-shrink: 0;
  position: relative;
`;

CreatePassword.DisabledButton = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
`;

export default CreatePassword;
