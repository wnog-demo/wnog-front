//base
export { default as Header } from './Header/Header';
export { default as Footer } from './Footer/Footer';
export { default as Layout } from './Layout';
export { default as Sidebar } from './Sidebar/Sidebar';
export { default as Content } from './Content/Content';
export { default as MainContainer } from './MainContainer/MainContainer';
export { default as PageWrapper } from './PageWrapper/PageWrapper';

//pages
export { default as Error } from './Error/Error';
export { default as Detail } from './Detail/Detail';
export { default as Settings } from './Settings/Settings';
export { default as Login } from './Login/Login';
export { default as Statuses } from './Statuses/Statuses';
export { default as StatusesTable } from './StatusesTable/StatusesTable';
export { default as CreatePassword } from './CreatePassword/CreatePassword';
