import React, { Component } from 'react';
import Block from './Typography.style';

export default class Typography extends React.PureComponent {
  render() {
    const { children } = this.props;
    const type = typeof children;

    return (
      <React.Fragment>
        {type === 'string' ? (
          <Block dangerouslySetInnerHTML={{ __html: children }} />
        ) : (
          <Block>{children}</Block>
        )}
      </React.Fragment>
    );
  }
}
