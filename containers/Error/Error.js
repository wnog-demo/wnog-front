import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Block from './Error.style';
import { Text, Icon, TextInput, Button } from 'elements';
import { Link } from 'lib/routes';
import { connect } from 'react-redux';
import { i18n } from 'lib/constants';

const messages = {
  404: 'not_found',
  500: 'server_not_responding'
};
const mapStateToProps = state => {
  return {
    app: state.app
  };
};
@connect(mapStateToProps)
export default class Error extends React.PureComponent {
  static propTypes = {
    code: PropTypes.number
  };
  render() {
    const {
      code,
      app: { lang }
    } = this.props;

    return (
      <Block.Root>
        <Block.Wrapper>
          {code && <Block.Background>{code}</Block.Background>}
          <Block.Headline>
            <Text view={'h1'}>
              {i18n('error', lang)}!
              <br />
              {i18n(messages[code], lang)}
            </Text>
          </Block.Headline>

          <Block.ButtonItem>
            <Link passHref route={'/'}>
              <Button view="large" as={'a'}>
                {i18n('to_home', lang)}
              </Button>
            </Link>
          </Block.ButtonItem>
        </Block.Wrapper>
      </Block.Root>
    );
  }
}
