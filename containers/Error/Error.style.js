import styled, { css } from 'styled-components';

const Error = {};

Error.Root = styled.div`
  display: flex;
  flex-grow: 1;
  margin-top: 100px;
  min-height: calc(100vh - 170px);
  overflow: hidden;
  ${({ theme: { color } }) => css`
    background-color: ${color.bgGray};
  `}
`;

Error.Wrapper = styled.div`
  position: relative;
  margin: 0 auto;
  display: flex;
  flex-grow: 1;
  flex-direction: column;
  align-items: center;
`;

Error.Background = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  z-index: 0;
  font-size: 470px;
  line-height: 1;
  font-weight: 900;
  user-select: none;
  display: flex;
  align-items: center;
  justify-content: center;
  transform: translateY(-50%);
  top: 50%;
  ${({ theme: { color } }) => css`
    color: ${color.white};
  `}
`;

Error.Headline = styled.div`
  margin-top: 9.25rem;
  text-align: center;
  max-width: 59%;
  margin-bottom: 2rem;
  z-index: 2;
  max-width: 500px;
`;

Error.ButtonItem = styled.div`
  display: flex;
  margin: 0 auto;
  flex-shrink: 0;
  z-index: 2;
`;

export default Error;
