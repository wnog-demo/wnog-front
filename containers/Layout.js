import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import Head from 'next/head';
import { bindActionCreators } from 'redux';
import { debounce } from 'throttle-debounce';
import { graphql, compose, withApollo } from 'react-apollo';
import { createGlobalStyle, ThemeProvider } from 'styled-components';

import actionsCreators from 'store/actions';
import { Login } from 'containers';
import { Notification } from 'elements';
import global from 'lib/styles/global';
import provider from 'lib/styles/provider';
import { IS_AUTH } from 'graphql/mutation';
import { logout, setCookie, getCookie, urlSearch } from 'lib/utils';
import { KEY } from 'lib/constants';

if (process.browser) {
  require('scrollingelement');
}

//page name
const noAuthPages = ['/createPassword', '/createInvite', '/resetPassword', '/restorePassword'];

const GlobalStyle = createGlobalStyle`
    ${global}
`;

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      changeDimensions: bindActionCreators(actionsCreators.changeDimensions, dispatch),
      changeNotificationState: bindActionCreators(actionsCreators.changeNotificationState, dispatch)
    }
  };
};

const mapStateToProps = state => {
  return {
    user: state.user,
    app: state.app,
    notification: state.notification
  };
};

@withRouter
@withApollo
@connect(
  mapStateToProps,
  mapDispatchToProps
)
export default class Layout extends React.PureComponent {
  constructor(props) {
    super(props);
    const { newLang } = props;
    if (process.browser) {
      const query = urlSearch.get('lang');
      if (query) {
        const lang = getCookie('language');
        if (lang !== newLang) {
          setCookie('language', newLang);
        }
        location.replace(urlSearch.remove('lang', location.href));
      }
    }
  }

  onClick = e => {
    const { app, actions } = this.props;
    // on global click!
  };

  componentWillUnmount() {
    window.removeEventListener('keydown', this.onKeyDown);
    window.removeEventListener('resize', this.onResize);
  }

  componentDidMount() {
    const { client, user } = this.props;
    window.addEventListener('keydown', this.onKeyDown);
    window.addEventListener('resize', this.onResize);
    this.onResize(null, true);

    //global one time actions
    user.token &&
      client
        .mutate({
          mutation: IS_AUTH
        })
        .then(res => {
          if (res.data && res.data.isAuth) {
          } else {
            logout();
          }
        });
  }

  onKeyDown = e => {
    const { actions, notification } = this.props;

    if (e.keyCode === KEY.ESC && notification.active) {
      actions.changeNotificationState(false);
    }
  };

  onResize = (e, force) => {
    if (force) {
      this.changeDimensions(e);
    } else {
      this.debonuceResize(e);
    }
  };

  debonuceResize = debounce(300, e => {
    this.changeDimensions(e);
  });

  changeDimensions() {
    const { actions } = this.props;
    const width = document.documentElement.clientWidth;
    const height = document.documentElement.clientHeight;

    actions.changeDimensions(width, height);
  }

  render() {
    const { app, children: page, user, router } = this.props;
    const isNoAuthPage = noAuthPages.includes(router.route);

    return (
      <main onClick={this.onClick}>
        <Head>
          <title>WNOG</title>
          <meta name="viewport" />
          <meta name="cmsmagazine" content="6b918355ad4ec31e713e8eefe2a5b74c" />
          <link href="/static/styles/fonts.css" rel="stylesheet" />
          <link href="/static/styles/reset.css" rel="stylesheet" />
          <link rel="icon" href={'/static/images/favicon.ico'} type="image/x-icon" />
          <link rel="shortcut icon" href={'/static/images/favicon.ico'} type="image/x-icon" />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href={'/static/images/favicon-16x16.png'}
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href={'/static/images/favicon-32x32.png'}
          />
          <link
            rel="icon"
            type="image/png"
            sizes="96x96"
            href={'/static/images/favicon-96x96.png'}
          />
          <script src="//cdn.polyfill.io/v2/polyfill.min.js?features=default,fetch,HTMLPictureElement&unknown=polyfill&flags=gated" />
        </Head>
        <GlobalStyle />
        <ThemeProvider theme={provider}>
          <React.Fragment>
            {isNoAuthPage || user.token ? page : <Login />}
            <Notification />
          </React.Fragment>
        </ThemeProvider>
      </main>
    );
  }
}
