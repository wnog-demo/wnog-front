import React, { Component } from 'react';

import { PageWrapper, Content } from 'containers';
import { Login } from 'components';

export default class LoginPage extends React.PureComponent {
  render() {
    return (
      <React.Fragment>
        <PageWrapper>
          <Content footerIsBlack={false}>
            <Login />
          </Content>
        </PageWrapper>
      </React.Fragment>
    );
  }
}
