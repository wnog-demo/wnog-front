import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'next/router';

import actionsCreators from 'store/actions';
import Block from './Header.style';
import { Text, Checkbox, Icon } from 'elements';
import { i18n } from 'lib/constants';
import { Link } from 'lib/routes';
import config from './config';

const mapStateToProps = state => {
  return {
    app: state.app,
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      changeLanguage: bindActionCreators(actionsCreators.changeLanguage, dispatch)
    }
  };
};

@withRouter
@connect(
  mapStateToProps,
  mapDispatchToProps
)
export default class Header extends React.PureComponent {
  onLanguageChange = e => {
    const { app } = this.props;

    this.changeLanguage(app.lang === 'ru' ? 'en' : 'ru');
  };

  changeLanguage(lang) {
    const { actions, app } = this.props;

    if (app.lang !== lang) {
      actions.changeLanguage(lang);
      location.reload();
    }
  }

  renderSvgCircle() {
    return (
      <svg viewBox="0 0 100 100" height="18px" width="18px" xmlns="http://www.w3.org/2000/svg">
        <circle cx="50" cy="50" r="18" />
      </svg>
    );
  }

  render() {
    const {
      user,
      app: { lang },
      router
    } = this.props;

    return (
      <Block.Root>
        <Block.Wrapper>
          <Block.InnerWrapper>
            <Link route={'/'} passHref>
              <Block.Logo>
                <img src="/static/images/wnog-logo.svg" />
              </Block.Logo>
            </Link>
            {user.token && (
              <Block.Nav>
                {config.map(item => {
                  const { name, route } = item;
                  const isActive = router.route === route;

                  return (
                    <Link passHref route={route} key={name}>
                      <Block.NavItem isActive={isActive} as={'a'}>
                        <Text view={'navigationLink'} as={'span'}>
                          {i18n(name, lang)}
                        </Text>
                      </Block.NavItem>
                    </Link>
                  );
                })}
              </Block.Nav>
            )}
          </Block.InnerWrapper>
          <Block.InnerWrapper>
            <a
              href={'https://www.wnoglogistics.com' + (lang === 'ru' ? '/ru' : '')}
              target="_blank"
            >
              <Block.About>{i18n('about_us', lang)}</Block.About>
            </a>

            <Block.Lang lang={lang}>
              <Block.LangItem as={'button'} onClick={e => this.changeLanguage('ru')}>
                <Text view={'lang'}>ru</Text>
              </Block.LangItem>
              <Checkbox onChange={this.onLanguageChange} value={lang === 'en'} />
              <Block.LangItem as={'button'} onClick={e => this.changeLanguage('en')}>
                <Text view={'lang'}>en</Text>
              </Block.LangItem>
            </Block.Lang>

            {user.token && (
              <Link route={'/settings'} passHref>
                <Block.Settings isActive={router.route === '/settingsPage'} as={'a'}>
                  {/* <Icon name="settings" /> */}
                  <Block.SettingsTop>
                    <Block.SettingsCircle>{this.renderSvgCircle()}</Block.SettingsCircle>
                  </Block.SettingsTop>
                  <Block.SettingsBottom>
                    <Block.SettingsCircle>{this.renderSvgCircle()}</Block.SettingsCircle>
                  </Block.SettingsBottom>
                </Block.Settings>
              </Link>
            )}
          </Block.InnerWrapper>
        </Block.Wrapper>
      </Block.Root>
    );
  }
}
