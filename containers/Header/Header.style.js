import styled, { css } from 'styled-components';

const Header = {};

Header.Root = styled.div`
  height: 100px;
  display: flex;
  align-items: center;
  position: fixed;
  width: 100%;
  max-width: 100%;
  z-index: 105;
  &:before {
    content: '';
    position: absolute;
    height: 2px;
    bottom: -2px;
    left: 0;
    width: 100%;
    background-color: #f9fafb;
  }
  ${({ theme: { color } }) => css`
    background-color: ${color.white};
  `}
  @media screen and (max-width: 1024px) {
    height: 80px;
  }
`;

Header.Wrapper = styled.div`
  width: 100%;
  max-width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-wrap: nowrap;
  padding: 0 4.5rem;
  @media screen and (max-width: 1024px) {
    padding: 0 2.5rem;
  }
  @media screen and (max-width: 600px) {
    padding: 0 1.5rem;
  }
`;

Header.InnerWrapper = styled.div`
  display: flex;
  align-items: center;
`;

Header.Logo = styled.a`
  cursor: pointer;
  width: 5rem;
  margin-right: 2rem;
  @media screen and (max-width: 400px) {
    width: 4.5rem;
    margin-right: 1rem;
  }
  img {
    width: 100%;
    position: relative;
    bottom: 0.125rem;
  }
`;

Header.Nav = styled.nav`
  display: flex;
  margin: 0 -1rem;
`;

Header.NavItem = styled.div`
  padding: 0 1rem;
  cursor: pointer;

  span {
    transition: all 0.2s ease;
  }

  ${({ isActive, theme: { color } }) => css`
    ${isActive &&
      css`
        color: ${color.blue};
      `}
    :hover,
    :focus {
      span {
        color: ${color.blue};
      }
    }
  `}
`;

Header.LangItem = styled.div`
  padding: 0 0.75rem;
  cursor: pointer;
  transition: all 0.2s ease;

  ${({ theme: { color } }) => css`
    :hover,
    :focus {
      color: ${color.blue};
    }
  `}
`;

Header.Lang = styled.div`
  display: flex;
  align-items: center;
  margin: 0 -0.75rem;
`;

Header.SettingsLine = styled.div`
  height: 2px;
  width: 20px;
  background-color: currentColor;
  position: relative;
  border-radius: 30px;
`;

Header.SettingsCircle = styled.div`
  position: absolute;
  height: 18px;
  width: 18px;
  top: -8px;
  svg {
    position: absolute;
    top: 0;
  }
`;

Header.SettingsTop = styled(Header.SettingsLine)`
  margin-bottom: 9px;
  ${Header.SettingsCircle} {
    right: -2px;
    transition: transform 0.2s ease;
    svg {
      fill: currentColor;
    }
  }
`;

Header.SettingsBottom = styled(Header.SettingsLine)`
  ${Header.SettingsCircle} {
    left: -2px;
    transition: transform 0.2s ease;
    svg {
      fill: currentColor;
    }
  }
`;

Header.About = styled.div`
  background-color: #3c7cb5;
  padding: 7px 25px;
  color: white;
  border-radius: 8px;
  text-transform: uppercase;
  font-size: 12px;
  font-weight: 500;
  letter-spacing: 0.025em;
  margin-right: 25px;
  cursor: pointer;
  transition: all 0.15s ease;
  &:hover {
    background-color: #252841;
    opacity: 0.85;
  }
  @media screen and (max-width: 400px) {
    padding: 0;
    color: #3c7cb5;
    background: none !important;
    margin-right: 20px;
  }
`;

Header.Settings = styled.button`
  margin-left: 2.125rem;
  transition: all 0.2s ease;
  font-size: 1em;

  @keyframes topAnimIn {
    0% {
      transform: none;
    }
    100% {
      transform: translateX(-6px);
    }
  }

  @keyframes topAnimOut {
    0% {
      transform: translateX(-6px);
    }
    100% {
      transform: none;
    }
  }

  @keyframes bottomAnimIn {
    0% {
      transform: none;
    }
    100% {
      transform: translateX(6px);
    }
  }

  @keyframes bottomAnimOut {
    0% {
      transform: translateX(6px);
    }
    100% {
      transform: none;
    }
  }

  ${Header.SettingsTop} {
    ${Header.SettingsCircle} {
      animation: topAnimOut 0.3s ease forwards;
    }
  }

  ${Header.SettingsBottom} {
    ${Header.SettingsCircle} {
      animation: bottomAnimOut 0.3s ease forwards;
    }
  }

  ${({ isActive, theme: { color } }) => css`
    ${isActive &&
      css`
        color: ${color.blue};
        ${Header.SettingsTop} {
          ${Header.SettingsCircle} {
            animation: topAnimIn 0.3s ease forwards;
          }
        }
        ${Header.SettingsBottom} {
          ${Header.SettingsCircle} {
            animation: bottomAnimIn 0.3s ease forwards;
          }
        }
      `}
    :hover {
      color: ${color.blue};
    }
  `}
`;

export default Header;
