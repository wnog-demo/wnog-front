import styled, { css } from 'styled-components';

const MainContainer = {};

MainContainer.Root = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  height: 100%;
`;

export default MainContainer;
