import React, { Component } from 'react';
import Block from './MainContainer.style';
import { CreatePassword } from 'components';

export default class MainContainer extends React.PureComponent {
  render() {
    return <Block.Root>{this.props.children}</Block.Root>;
  }
}
