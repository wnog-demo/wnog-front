import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'next/router';

import { MainContainer, Header, Footer, Content } from 'containers';
import Block from './PageWrapper.style';

@withRouter
export default class PageWrapper extends React.PureComponent {
  static propTypes = {
    sidebar: PropTypes.any
  };
  render() {
    const { sidebar } = this.props;

    return (
      <MainContainer>
        <Header />
        {this.props.children}
        {/* {sidebar && <Sidebar>{sidebar}</Sidebar>} */}
      </MainContainer>
    );
  }
}
