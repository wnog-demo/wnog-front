import styled, { css } from 'styled-components';

const Statuses = {};

Statuses.Root = styled.div`
  position: relative;
`;

Statuses.Wrapper = styled.div`
  padding: 40px 20px 40px 4.5rem;
`;

Statuses.BottomLoader = styled.div`
  position: absolute;
  left: 0;
  bottom: 30px;
  width: 100%;
  text-align: center;
  z-index: 100;
`;

Statuses.DetailPreview = styled.div`
  position: fixed;
  left: 0;
  width: 100%;
  z-index: 100;
  top: -300%;
  height: calc(100vh - 100px);
  transition: top 0s ease 0.6s, opacity 0.4s ease 0.2s;
  opacity: 0;

  @media screen and (max-width: 1024px) {
    height: calc(100vh - 80px);
  }

  ${({ isActive, enableScrolling }) => css`
    ${enableScrolling &&
      css`
        position: relative;
      `}

    ${isActive &&
      css`
        transition: none;
        top: 100px;
        opacity: 1;
        @media screen and (max-width: 1024px) {
          top: 80px;
        }
      `}
    &:before {
      content: '';
      position: absolute;
      height: 100%;
      width: 100%;
      top: 0;
      left: 0;
      transition: all 0.5s ease-in;
      background-color: white;
      opacity: 0;
      ${isActive &&
        css`
          opacity: 1;
          background-color: white;
          transition: all 0.5s ease;
        `}
    }
  `}
`;

export default Statuses;
