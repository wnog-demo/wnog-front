import React, { Component } from 'react';
import { graphql, compose, withApollo, Query } from 'react-apollo';
import { connect } from 'react-redux';
import { debounce } from 'throttle-debounce';
import { bindActionCreators } from 'redux';
import { withRouter } from 'next/router';

import actionsCreators from 'store/actions';
import { PageWrapper, StatusesTable, Sidebar, Content, Detail } from 'containers';
import { StatusSidebar, DetailSidebar } from 'components';
import { Text, Button, Loader } from 'elements';
import { GET_CUSTOM_CLEARANCE, GET_FILTER } from 'graphql/query';
import { getScrollPosition } from 'lib/utils';
import Block from './Statuses.style';
import { i18n } from 'lib/constants';
import provider from 'lib/styles/provider';

const LIMIT = 15;

const mapStateToProps = state => {
  return {
    filter: state.filter,
    app: state.app
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      resetFilter: bindActionCreators(actionsCreators.resetFilter, dispatch),
      changeClearanceOffset: bindActionCreators(actionsCreators.changeClearanceOffset, dispatch),
      updateLastScroll: bindActionCreators(actionsCreators.updateLastScroll, dispatch)
    }
  };
};

@withRouter
@withApollo
@connect(
  mapStateToProps,
  mapDispatchToProps
)
@compose(graphql(GET_FILTER, { name: 'filterData' }))
export default class Statuses extends React.PureComponent {
  lastScroll = 0;
  offsetFetchIsNecessary = true;
  lastScrollTimeout = null;
  firstLoad = true;
  rowRefs = {};
  bounds = null;
  detailHeader = null;
  detailHeaderBounds = null;
  timers = {};

  constructor(props) {
    super(props);

    const uid = props.router.query.uid;

    this.state = {
      offset: 0,
      data: null,
      activeUid: uid,
      transitionIsActive: false
    };
  }

  componentDidMount() {
    const { filter } = this.props;

    window.addEventListener('scroll', this.onScroll, { passive: true });
    window.addEventListener('mousewheel', this.onMouseWheel, { passive: true });

    console.log('1');
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll);
    window.removeEventListener('mousewheel', this.onMouseWheel);

    Object.values(this.timers).forEach(timer => {
      clearTimeout(timer);
    });
  }

  onMouseWheel = e => {
    const delta = Math.max(-1, Math.min(1, e.wheelDelta || -e.detail));
    if (delta === -1) {
      this.onScroll();
    }
  };

  onScroll = e => {
    const { app, router } = this.props;
    const scroll = getScrollPosition();

    const detailIsActive = !!router.query.uid;

    if (detailIsActive) return false;

    const { scrollHeight } = document.documentElement;

    if (scroll >= 50) {
      document.body.classList.add('scrolled-50px');
    } else {
      document.body.classList.remove('scrolled-50px');
    }

    if (scroll + app.height >= scrollHeight - app.height / 2) {
      this.loadMore();
    }
  };

  loadMore() {
    const { filter, app, actions } = this.props;

    if (this.offsetFetchIsNecessary) {
      this.offsetFetchIsNecessary = false;
      actions.changeClearanceOffset(filter.clearanceOffset + LIMIT);
    }
  }

  onResetFilter = e => {
    const { actions } = this.props;

    actions.resetFilter();
  };

  formatDate(value) {
    if (!value) return;

    if (value === true) {
      return JSON.stringify({
        from: 0,
        to: 0
      });
    } else {
      return JSON.stringify({
        from: value.from ? Math.floor(new Date(value.from) / 1000) : 0,
        to: value.to ? Math.floor(new Date(value.to) / 1000) : 0
      });
    }
  }

  onLinkClick = (event, uid, node) => {
    if (this.state.transitionIsActive) return false;

    event && event.preventDefault();
    const { router } = this.props;

    this.lastScroll = getScrollPosition();

    const row = this.rowRefs[uid];

    this.setState(
      {
        activeUid: uid
      },
      e => {
        this.bounds = row && row.getBoundingClientRect();
        this.detailHeaderBounds = this.detailHeader && this.detailHeader.getBoundingClientRect();

        this.setState({
          transitionIsActive: true
        });

        this.timers['transition-in'] = setTimeout(e => {
          node && node.click();
          this.setState({
            transitionIsActive: false
          });
        }, 600);
      }
    );

    // router.push(`/clearance/${uid}`);
  };

  getDetailHeaderRef = node => {
    this.detailHeader = node;
  };

  onCloseClick = e => {
    this.timers['setLastScroll'] = setTimeout(e => {
      window.scrollTo(0, this.lastScroll);
    }, 100);
  };

  renderDetailPreview(data) {
    const { activeUid, transitionIsActive } = this.state;
    const { router } = this.props;

    const isActive = transitionIsActive || !!router.query.uid;
    const enableScrolling = !!router.query.uid;

    return (
      <Block.DetailPreview isActive={isActive} enableScrolling={enableScrolling}>
        <Detail
          isActive={isActive}
          data={data}
          bounds={this.bounds}
          getHeaderRef={this.getDetailHeaderRef}
          headerBounds={this.detailHeaderBounds}
          onCloseClick={this.onCloseClick}
        />
      </Block.DetailPreview>
    );
  }

  getRowRef = (uid, node) => {
    this.rowRefs[uid] = node;
  };

  render() {
    const {
      filterData,
      filter,
      app: { lang },
      router
    } = this.props;

    const activeUid = this.state.activeUid || router.query.uid;

    const ata = filter.options.ata ? this.formatDate(filter.options.ata) : null;
    const eta = filter.options.eta ? this.formatDate(filter.options.eta) : null;

    return (
      <Query
        query={GET_CUSTOM_CLEARANCE}
        variables={{
          limit: filter.clearanceOffset + LIMIT,
          query: filter.query,
          type: filter.options.types,
          customs: filter.options.customs,
          ata: ata,
          eta: eta,
          greenlights: filter.options.greenlights,
          transportations: filter.options.transportations,
          declarations: filter.options.declarations,
          is_archive: filter.options.is_archive
        }}
        onCompleted={data => {
          if (
            !data.customClearance ||
            filter.clearanceOffset + LIMIT > data.customClearance.length
          ) {
            this.offsetFetchIsNecessary = false;
          } else {
            this.offsetFetchIsNecessary = true;
          }
        }}
      >
        {({ data: _data, loading, error }) => {
          const data = _data && _data.customClearance;

          if (!loading && this.firstLoad) {
            this.firstLoad = false;
          }

          const activeDetailPreview =
            activeUid && data && !!data.length && data.find(item => item.uid === activeUid);

          const disableScrolling = !!router.query.uid;

          return (
            <React.Fragment>
              {this.renderDetailPreview(activeDetailPreview)}
              <Content sidebar={true} disableScrolling={disableScrolling}>
                <>
                  {data && !!data.length && (
                    <StatusesTable
                      onLinkClick={this.onLinkClick}
                      data={data}
                      getRowRef={this.getRowRef}
                    />
                  )}
                  {data && !data.length && (
                    <Block.Wrapper>
                      <Text view={'h2'}>{i18n('no_requests_parameters', lang)}</Text>
                      <br />
                      <Button onClick={this.onResetFilter} view={'common'}>
                        {i18n('all_request', lang)}
                      </Button>
                    </Block.Wrapper>
                  )}
                  {this.firstLoad && loading && (
                    <Block.Wrapper>
                      <Text>{i18n('loading', lang)}...</Text>
                    </Block.Wrapper>
                  )}
                  {loading && (
                    <Block.BottomLoader>
                      <Loader />
                    </Block.BottomLoader>
                  )}
                </>
                <Sidebar>
                  <StatusSidebar data={filterData} />
                </Sidebar>
              </Content>
            </React.Fragment>
          );
        }}
      </Query>
    );
  }
}
