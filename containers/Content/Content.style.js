import styled, { css } from 'styled-components';

const Content = {};

Content.Root = styled.div`
  ${({ sidebar, disableScrolling }) => css`
    ${sidebar &&
      css`
        margin-right: 380px;
      `}
    ${disableScrolling &&
      css`
        display: none;
      `}
  `}
  flex-direction: column;
  margin-top: 100px;
  @media screen and (max-width: 1024px) {
    margin-top: 80px;
  }
`;

Content.Content = styled.div`
  min-height: calc(100vh - 170px);
  position: relative;
  @media screen and (max-width: 1024px) {
    min-height: calc(100vh - 160px);
  }
  ${({ theme: { color } }) => css`
    background-color: ${color.bgGray};
  `}
`;

Content.Footer = styled.div`
  flex-shrink: 0;
`;

export default Content;
