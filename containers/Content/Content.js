import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Footer } from 'containers';
import Block from './Content.style';

export default class Content extends React.PureComponent {
  static propTypes = {
    sidebar: PropTypes.bool,
    footerIsBlack: PropTypes.bool,
    disableScrolling: PropTypes.bool
  };

  static defaultProps = {
    children: '',
    footerIsBlack: true
  };
  render() {
    const { sidebar, footerIsBlack, disableScrolling } = this.props;

    return (
      <Block.Root sidebar={sidebar} disableScrolling={disableScrolling}>
        <Block.Content>{this.props.children}</Block.Content>
        <Footer footerIsBlack={footerIsBlack} />
      </Block.Root>
    );
  }
}
