import React, { Component } from 'react';
import Block from './Sidebar.style';

export default class Sidebar extends React.PureComponent {
  static defaultProps = {
    children: ''
  };
  render() {
    return <Block.Root>{this.props.children}</Block.Root>;
  }
}
