import styled, { css } from 'styled-components';

const Sidebar = {};

Sidebar.Root = styled.div`
  width: 380px;
  position: fixed;
  top: 100px;
  right: 0;
  bottom: 0;
  height: auto;
  ${({ theme: { color } }) => css`
    background-color: ${color.sidebarBgGray};
  `}
`;

export default Sidebar;
