import styled, { css, keyframes } from 'styled-components';

const Detail = {};

Detail.Root = styled.div``;

Detail.Wrapper = styled.div`
  position: relative;
  min-height: calc(100vh - 170px);
  @media screen and (max-width: 1024px) {
    min-height: calc(100vh - 160px);
  }
`;

Detail.RowAnimation = styled.div`
  position: absolute;
  left: 0;
  width: 100%;
  z-index: 10;

  ${({ theme: { color, props } }) => {
    const bounds = props.bounds || {};
    const headerBounds = props.headerBounds || {};

    const anim = keyframes`
      0%{
        top: ${bounds.top - 100}px;
        height: ${bounds.height}px;
      }
      70%{
        opacity: 1;
        visibility: visible;
        top: 0;
        height: ${headerBounds.height}px;
      }
      100%{
        top: 0;
        height: ${headerBounds.height}px;
        opacity: 0;
        visibility: hidden;
      }
    `;

    return css`
      background-color: ${color.darkBlue};
      ${props.isActive &&
        css`
          animation: ${anim} 0.5s ease forwards;
        `}
    `;
  }}
`;

Detail.Header = styled.div`
  position: relative;
  z-index: 11;
  transition: background-color 0s ease 0.6s, transform 0.6s ease;
  transform: translateY(-250px);

  > div {
    opacity: 0;
    transition: all 0.3s ease 0.2s;
  }

  ${({ theme: { color, props } }) => css`
    //background-color: ${color.darkBlue};
    ${props.isActive &&
      css`
        transform: none;
        transition: background-color 0s ease 0.3s, transform 0s ease;
        background-color: ${color.darkBlue};
        > div {
          transition: all 0.4s ease-in 0.2s;
          opacity: 1;
        }
      `}
  `}
`;

Detail.ContentWrapper = styled.div`
  flex-grow: 1;
  background-color: white;

  transform: translateY(150px);
  transition: transform 0.5s ease, opacity 0.5s ease;
  opacity: 0;

  ${({ theme: { color, props } }) => css`
    ${props.isActive &&
      css`
        transition: all 0.4s ease;
        transform: none;
        opacity: 1;
      `}
  `}
`;

Detail.TransportInfo = styled.div`
  width: 100%;
  max-width: 1100px;
  padding: 2.5rem 3rem 2.5rem 15.75rem;
  margin: 0 auto;
  display: flex;
  align-items: flex-start;
`;

Detail.TransportHeadline = styled.div`
  margin-top: -0.55rem;
  width: 30%;
`;

Detail.TransportEvents = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
`;

Detail.EventItem = styled.div`
  display: flex;
  align-items: flex-start;
  margin-bottom: 1rem;
`;

Detail.DateWrapper = styled.div`
  width: 20%;
  display: flex;
`;

Detail.Date = styled.div`
  flex-shrink: 0;
  margin-right: 1.5rem;
  margin-left: auto;
  border-radius: 16px;
  padding: 2px 6px;
  ${({ theme: { color } }) => css`
    background-color: ${color.bgGray};
  `}
`;

Detail.StateWrapper = styled.div`
  text-align: left;
  width: 65%;
  display: flex;
  flex-direction: column;
  align-self: flex-start;
`;

Detail.State = styled.div`
  text-align: left;
  font-size: 0;

  > span {
    line-height: 16px;
  }
`;

Detail.Note = styled.div`
  margin-top: 0.5rem;
`;

Detail.Content = styled.div`
  margin-right: 380px;
  position: relative;
  z-index: 2;
`;

Detail.Error = styled.div`
  position: relative;
  z-index: 5;
  padding: 70px;
  font-weight: bold;
  font-size: 11px;
  line-height: 12px;
  letter-spacing: 0.1em;
  opacity: 0.9;
  text-transform: uppercase;
`;

Detail.Sidebar = styled.div`
  width: 380px;
  position: fixed;
  top: 100px;
  right: 0;
  bottom: 0;
  height: auto;
  transform: translateX(100%);
  visibility: hidden;
  transition: transform 0.4s ease, visibility 0s ease 0.4s;

  @media screen and (max-width: 1024px) {
    top: 80px;
  }

  ${({ theme: { color, props } }) => css`
    background-color: ${color.sidebarBgGray};
    ${props.isActive &&
      css`
        transition: transform 0.4s ease, visibility 0s ease;
        visibility: visible;
        transform: none;
      `}
  `}
`;

Detail.Tabs = styled.div`
  display: flex;
  padding-left: 10.5rem;
  padding-top: 2rem;
  max-width: 1100px;
  margin: 0 auto;
  align-items: center;
`;

Detail.TabLink = styled.div`
  color: #2cabfc;
  font-size: 11px;
  text-transform: uppercase;
  letter-spacing: 0.01em;
  margin-right: 3rem;
  cursor: pointer;
  user-select: none;
  padding-bottom: 5px;
  letter-spacing: 0.12em;
  font-weight: 600;
  ${({ isActive, isDisabled }) => css`
    ${isDisabled &&
      css`
        pointer-events: none;
        opacity: 0.3;
        color: black;
      `}
    ${isActive &&
      css`
        border-bottom: 2px solid;
      `}
  `}
`;

export default Detail;
