import React, { Component } from 'react';
import { withRouter } from 'next/router';
import { graphql, compose, withApollo } from 'react-apollo';
import { ThemeProvider } from 'styled-components';
import { connect } from 'react-redux';
import moment from 'moment';

import { PageWrapper, Footer } from 'containers';
import { DetailHeader, DetailContent, DetailSidebar, DetailStatuses, Documents } from 'components';
import { Text, Icon, TextInput, Button } from 'elements';
import { GET_CUSTOM_CLEARANCE } from 'graphql/query';
import { throwErrorPage, getLast, formatTime } from 'lib/utils';
import Block from './Detail.style';
import { i18n, TRANSPORTATION_STATUSES } from 'lib/constants';

const mapStateToProps = state => {
  return {
    app: state.app
  };
};

@connect(mapStateToProps)
@withRouter
@compose(
  graphql(GET_CUSTOM_CLEARANCE, {
    name: 'customClearanceData',
    options: props => {
      const uid = props.router.query.uid;

      return {
        variables: {
          uid: uid || 'xxx-999'
        }
      };
    }
  })
)
export default class Detail extends React.PureComponent {
  state = {
    activeTab: 'statuses'
  };

  get data() {
    const { data: dataFromProps, customClearanceData } = this.props;

    const data =
      !customClearanceData.loading &&
      customClearanceData.customClearance &&
      customClearanceData.customClearance[0];

    return dataFromProps || data;
  }

  onTabClick = name => {
    this.setState({
      activeTab: name
    });
  };

  componentDidUpdate(prevProps) {
    if (!prevProps.isActive && this.props.isActive && this.state.activeTab !== 'statuses') {
      this.setState({
        activeTab: 'statuses'
      });
    }
  }

  renderMain() {
    const data = this.data;
    const {
      reference,
      date,
      product,
      shipper,
      consignee,
      shipments,
      contact,
      coordinator,
      stages,
      custom,
      route,
      transportations,
      delivery,
      type,
      vessel,
      documents
    } = data;

    const {
      app: { lang },
      isActive,
      bounds,
      headerBounds,
      getHeaderRef,
      router,
      onCloseClick
    } = this.props;

    const providerProps = {
      isActive,
      bounds,
      headerBounds
    };

    const uid = router.query.uid;

    const { activeTab } = this.state;

    const docLength =
      documents &&
      !!documents.length &&
      documents.reduce((acc, cur) => {
        const length = cur.documents && cur.documents.length;
        return acc + (length ? length : 0);
      }, 0);

    return (
      <ThemeProvider theme={{ props: providerProps }}>
        <>
          <Block.Content>
            <React.Fragment>
              <Block.Wrapper>
                <Block.RowAnimation />
                <Block.Header ref={getHeaderRef}>
                  <DetailHeader
                    reference={reference}
                    creationDate={date}
                    product={product && product.name}
                    from={shipper && shipper.name}
                    to={consignee && consignee.name}
                    contact={contact}
                    shipments={shipments}
                    coordinator={coordinator && coordinator.name}
                    onCloseClick={onCloseClick}
                    isActive={isActive}
                    clickedRowBounds={bounds}
                  />
                </Block.Header>
                <Block.ContentWrapper>
                  {
                    <Block.Tabs>
                      {['statuses', 'documents'].map(tabName => {
                        const isActive = activeTab === tabName;
                        const documentsIsDisabled = !documents || !documents.length;
                        const isDisabled = tabName === 'documents' && documentsIsDisabled;
                        let name = i18n(tabName, lang);
                        if (tabName === 'documents') {
                          if (documentsIsDisabled) {
                            name = i18n('empty_documents', lang);
                          } else {
                            name = name + ` (${docLength})`;
                          }
                        }

                        return (
                          <Block.TabLink
                            isActive={isActive}
                            onClick={e => this.onTabClick(tabName)}
                            isDisabled={isDisabled}
                          >
                            {name}
                          </Block.TabLink>
                        );
                      })}
                    </Block.Tabs>
                  }
                  {documents && activeTab === 'documents' && (
                    <Documents documents={documents} lang={lang} />
                  )}
                  {stages && activeTab === 'statuses' && (
                    <DetailStatuses stages={stages} key={uid} />
                  )}
                  {transportations && activeTab === 'statuses' && !!transportations.length && (
                    <Block.TransportInfo>
                      <Block.TransportHeadline>
                        <Text view={'detailHeadline'}>{i18n('transportation', lang)}</Text>
                      </Block.TransportHeadline>
                      <Block.TransportEvents>
                        {transportations.map(item => {
                          const { date: _date, title, comment: _comment, slug } = item;
                          const date = _date
                            ? moment(formatTime(_date)).format('DD.MM.YY')
                            : undefined;

                          const containsShipped = transportations.some(
                            item => item.slug === TRANSPORTATION_STATUSES.SHIPPED
                          );

                          const isShipped = containsShipped
                            ? TRANSPORTATION_STATUSES.SHIPPED === slug
                            : TRANSPORTATION_STATUSES.CONFIRMED === slug;

                          const comment = isShipped ? delivery && delivery.name : _comment;

                          return (
                            <Block.EventItem key={slug}>
                              <Block.DateWrapper>
                                {date && (
                                  <Block.Date>
                                    <Text view={'date'}>{date}</Text>
                                  </Block.Date>
                                )}
                              </Block.DateWrapper>
                              <Block.StateWrapper>
                                <Block.State>
                                  <Text view={'bold'}>{title}</Text>
                                </Block.State>
                                {comment && (
                                  <Block.Note>
                                    <Text view={'note'}>{comment}</Text>
                                  </Block.Note>
                                )}
                              </Block.StateWrapper>
                            </Block.EventItem>
                          );
                        })}
                      </Block.TransportEvents>
                    </Block.TransportInfo>
                  )}
                </Block.ContentWrapper>
              </Block.Wrapper>
              <Footer footerIsBlack={true} />
            </React.Fragment>
          </Block.Content>
          <Block.Sidebar>
            <DetailSidebar type={type} vessel={vessel} custom={custom} route={route} />
          </Block.Sidebar>
        </>
      </ThemeProvider>
    );
  }

  render() {
    const { customClearanceData } = this.props;

    if (!customClearanceData.loading && !this.data) {
      return <Block.Error>Такой страницы не существует</Block.Error>;
    }

    return <Block.Root>{this.data && this.renderMain()}</Block.Root>;
  }
}
