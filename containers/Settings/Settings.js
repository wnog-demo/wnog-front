import React, { Component } from 'react';
import { graphql, compose, withApollo } from 'react-apollo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import actionsCreators from 'store/actions';
import { getUserData } from 'graphql/selectors';
import { Text, Icon, TextInput, Button, Checkbox, Loader } from 'elements';
import { ChangePassword } from 'components';
import { PageWrapper, Content } from 'containers';
import { logout } from 'lib/utils';
import { i18n } from 'lib/constants';
import Block from './Settings.style';
import { GET_NOTIFICATIONS, GET_USER } from 'graphql/query';
import { UPDATE_NOTIFICATIONS } from 'graphql/mutation';

const mapStateToProps = state => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      showNotification: bindActionCreators(actionsCreators.showNotification, dispatch)
    }
  };
};

@connect(
  mapStateToProps,
  mapDispatchToProps
)
@compose(
  graphql(GET_USER, { name: 'userData' }),
  graphql(GET_NOTIFICATIONS, { name: 'notificationsData' })
)
@withApollo
export default class Settings extends React.PureComponent {
  onNotificationChange = (id, value) => {
    const {
      client,
      actions,
      notificationsData,
      app: { lang }
    } = this.props;
    const values = JSON.stringify({
      [id]: !value
    });

    client
      .mutate({
        mutation: UPDATE_NOTIFICATIONS,
        variables: {
          values
        }
      })
      .then(res => {
        const result = res.data && res.data.updateNotifications;
        if (result) {
          actions.showNotification(i18n('settings_saved', lang));
          notificationsData.refetch();
        } else {
          actions.showNotification(i18n('settings_error'), 'error');
        }
      });
  };

  logout = e => {
    logout();
  };

  getNotificationData = e => {
    const { notificationsData: data } = this.props;

    return (!data.loading && data.notification) || [];
  };

  renderNotifications = e => {
    const data = this.getNotificationData();

    return (
      <Block.SettingsInnerWrapper>
        {data.map(item => {
          const { id, name, value } = item;

          return (
            <Block.NotificationsItem key={id}>
              <Checkbox
                view={'blue'}
                onChange={e => this.onNotificationChange(id, value)}
                value={value}
              >
                <Text view={'bold'} as={'span'}>
                  {name}
                </Text>
              </Checkbox>
            </Block.NotificationsItem>
          );
        })}
      </Block.SettingsInnerWrapper>
    );
  };

  render() {
    const {
      userData,
      notificationsData,
      app: { lang }
    } = this.props;
    const userInfo = getUserData(userData.user);

    return (
      <Content sidebar={false}>
        <Block.Root>
          <Block.MainWrapper>
            <Block.Wrapper>
              <Block.Headline>
                <Text view={'h1Left'}>{i18n('setting', lang)}</Text>
              </Block.Headline>
              <Block.SettingsWrapper>
                <Block.Data>
                  <Block.Title>
                    <Text view={'upperSpacing'}>{i18n('your_data', lang)}</Text>
                  </Block.Title>
                  <Block.SettingsInnerWrapper>
                    {userInfo.name && (
                      <Block.DataItem>
                        <Text view={'bold'}>{userInfo.name}</Text>
                      </Block.DataItem>
                    )}
                    {userInfo.client && (
                      <Block.DataItem>
                        <Text view={'bold'}>{userInfo.client.name}</Text>
                      </Block.DataItem>
                    )}
                    {userInfo.email && (
                      <Block.DataItem>
                        <Text view={'bold'}>{userInfo.email}</Text>
                      </Block.DataItem>
                    )}
                    <Block.Manual as={'a'} href={'/static/manual.pptx'} target={'_blank'}>
                      <Text view={'upperSpacing'} as={'span'}>
                        {i18n('download_manual', lang)}
                      </Text>
                    </Block.Manual>
                    <Block.Manual
                      as={'a'}
                      href={'https://readymag.com/nimaxagency/ewnog-short/'}
                      target={'_blank'}
                    >
                      <Text view={'upperSpacing'} as={'span'}>
                        {i18n('online_manual', lang)}
                      </Text>
                    </Block.Manual>
                    <Block.Logout onClick={this.logout}>
                      <Block.LogoutIcon>
                        <Icon name="logout" />
                      </Block.LogoutIcon>
                      <Text view={'upperSpacing'} as={'span'}>
                        {i18n('sign_out', lang)}
                      </Text>
                    </Block.Logout>
                  </Block.SettingsInnerWrapper>
                </Block.Data>
                <Block.Notifications>
                  <Block.Title>
                    <Text view={'upperSpacing'}>{i18n('notify_by_mail', lang)}</Text>
                  </Block.Title>
                  {notificationsData.loading && <Loader />}
                  {!notificationsData.loading && this.renderNotifications()}
                </Block.Notifications>
              </Block.SettingsWrapper>
            </Block.Wrapper>
          </Block.MainWrapper>
          <Block.ChangePassword>
            <ChangePassword />
          </Block.ChangePassword>
        </Block.Root>
      </Content>
    );
  }
}
