import styled, { css } from 'styled-components';

const Settings = {};

Settings.Root = styled.div``;

Settings.MainWrapper = styled.div`
  width: 100%;
  ${({ theme: { color } }) => css`
    border-bottom: 4px solid ${color.white};
  `}
`;

Settings.Wrapper = styled.div`
  width: 100%;
  max-width: 1100px;
  padding: 6rem 3rem 6.25rem 10.5rem;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
`;

Settings.Headline = styled.div`
  max-width: 59%;
  margin-bottom: 3.5rem;
`;

Settings.SettingsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
`;

Settings.SettingsInnerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
`;

Settings.Title = styled.div`
  opacity: 0.6;
  margin-bottom: 2rem;
`;

Settings.Data = styled.div`
  width: 50%;
  padding-right: 40px;
  display: flex;
  flex-direction: column;
`;

Settings.DataItem = styled.div`
  margin-bottom: 1.5rem;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

Settings.Logout = styled.button`
  margin-top: auto;
  display: flex;
  margin-bottom: 1.5rem;
  align-items: center;
  ${({ theme: { color } }) => css`
    > span {
      margin-left: 0.7rem;
      color: ${color.blue};
      transition: all 0.2s ease;
    }

    :hover,
    :focus {
      > span {
        color: ${color.black};
        opacity: 0.85;
      }
    }
  `}
`;

Settings.Manual = styled.button`
  ${({ theme: { color } }) => css`
    color: ${color.blue};
    :hover,
    :focus {
      > span {
        color: ${color.black};
        opacity: 0.85;
      }
    }
  `}
`;

Settings.LogoutIcon = styled.div`
  ${({ theme: { color } }) => css`
    color: ${color.blue};
  `}
`;

Settings.Notifications = styled(Settings.Data)``;

Settings.NotificationsItem = styled(Settings.DataItem)`
  span {
    margin-left: 1.5rem;
  }
`;

Settings.ChangePassword = styled.div`
  display: flex;
  justify-content: center;
`;

Settings.InputWrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
`;

Settings.InputItem = styled.div`
  display: flex;
  margin-right: 20px;
  flex-grow: 1;
  flex-shrink: 0;
`;

Settings.ButtonItem = styled.div`
  display: flex;
  flex-shrink: 0;
`;

export default Settings;
