import React, { Component } from 'react';
import { Text, TextInput, Button } from 'elements';
import { PageWrapper } from 'containers';

export default class CreateInvite extends React.PureComponent {
  state = {
    email: ''
  };
  onChange = value => {
    this.setState({
      email: value
    });
  };
  onSend = e => {
    fetch('https://api.develop.wnog.dev.nimax.ru/api/user', {
      method: 'POST',
      body: JSON.stringify({
        uid: `123125-2342342-${parseInt(Math.random() * 99)}`,
        role: 1,
        name: 'Имя',
        last_name: 'Фамилия',
        en_name: 'Name',
        en_last_name: 'Last name',
        email: this.state.email.trim(),
        company: 'Company name'
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(e => console.log(e));
  };
  render() {
    return (
      <React.Fragment>
        <PageWrapper>
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <div style={{ textAlign: 'center' }}>
            <div>Создать инвайт</div>
            <br />
            <div style={{ display: 'inline-block' }}>
              <TextInput onChange={this.onChange} value={this.state.email} placeholder={'Email'} />
            </div>
            <br />
            <br />
            <div style={{ display: 'inline-block' }}>
              <Button onClick={this.onSend} view={'common'}>
                Отправить
              </Button>
            </div>
          </div>
        </PageWrapper>
      </React.Fragment>
    );
  }
}
