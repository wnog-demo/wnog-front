import React from 'react';
import { Provider, connect } from 'react-redux';
import App, { Container } from 'next/app';
import withRedux from 'next-redux-wrapper';
import { ApolloProvider } from 'react-apollo';
import nextWithApollo from 'lib/next-with-apollo';
import { initStore } from '../store';

import { Layout, PageWrapper } from 'containers';
import { getCookie } from 'lib/utils';
import moment from 'moment';

@nextWithApollo
@withRedux(initStore)
export default class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let cookieStore = ctx && ctx.req && ctx.req.headers.cookie ? ctx.req.headers.cookie : ' ';
    const auth_token = getCookie('auth_token', cookieStore);
    const language = getCookie('language', cookieStore);

    auth_token && ctx.store.dispatch({ type: 'CHANGE_TOKEN', token: auth_token });

    const lang = ctx.query.lang || language;
    if (lang) {
      ctx.store.dispatch({ type: 'CHANGE_LANGUAGE', lang });
    }

    moment.locale(lang || 'ru');

    return {
      pageProps: Component.getInitialProps ? await Component.getInitialProps(ctx) : {},
      lang
    };
  }
  render() {
    const { Component, pageProps, store, apollo, lang, router } = this.props;

    return (
      <Container>
        <Provider store={store}>
          <ApolloProvider client={apollo}>
            <Layout newLang={lang}>
              <PageWrapper>
                <Component {...pageProps} />
              </PageWrapper>
            </Layout>
          </ApolloProvider>
        </Provider>
      </Container>
    );
  }
}
