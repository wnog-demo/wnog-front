import React, { Component } from 'react';
import CreatePassword from './createPassword';
import { Content } from 'containers';

export default class CreatePasswordPage extends React.PureComponent {
  render() {
    return (
      <React.Fragment>
        <Content footerIsBlack={false}>
          <CreatePassword />
        </Content>
      </React.Fragment>
    );
  }
}
