import React, { Component } from 'react';
import { PageWrapper, Content } from 'containers';
import { PasswordReset } from 'components';

export default class ResetPassword extends React.PureComponent {
  render() {
    return (
      <React.Fragment>
        <Content footerIsBlack={false}>
          <PasswordReset />
        </Content>
      </React.Fragment>
    );
  }
}
