import React from 'react';
import Head from 'next/head';
import { Header, Footer, MainContainer, Error } from 'containers';
import { withRouter } from 'next/router';

@withRouter
export default class ErrorPage extends React.Component {
  static propTypes() {
    return {
      errorCode: React.PropTypes.number.isRequired,
      url: React.PropTypes.string.isRequired
    };
  }

  static getInitialProps({ res, xhr }) {
    const errorCode = res ? res.statusCode : xhr ? xhr.status : null;
    return { errorCode };
  }

  render() {
    return (
      <MainContainer>
        <Header />
        <Error code={this.props.errorCode} />
        <Footer />
      </MainContainer>
    );
  }
}
