import React, { Component } from 'react';
import { Settings } from 'containers';

export default class SettingsPage extends React.PureComponent {
  render() {
    return (
      <React.Fragment>
        <Settings />
      </React.Fragment>
    );
  }
}
