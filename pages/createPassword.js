import React, { Component } from 'react';
import { PageWrapper, Content } from 'containers';
import { CreatePassword } from 'containers';

export default class CreatePasswordPage extends React.PureComponent {
  render() {
    return (
      <React.Fragment>
        <Content footerIsBlack={false}>
          <CreatePassword />
        </Content>
      </React.Fragment>
    );
  }
}
