import gql from 'graphql-tag';

export const ProductFragment = gql`
  fragment Product on Product {
    uid
    name
  }
`;

export const CustomFragment = gql`
  fragment Custom on Custom {
    uid
    name
    city
    fullname
  }
`;

export const ShipperFragment = gql`
  fragment Shipper on Shipper {
    uid
    name
    address
  }
`;

export const PlaceFragment = gql`
  fragment Place on Place {
    uid
    title
    name
    address
    type
  }
`;

export const RouteFragment = gql`
  fragment Route on Route {
    type
    number
    list {
      ...Place
    }
  }
  ${PlaceFragment}
`;

export const ShipmentFragment = gql`
  fragment Shipment on Shipment {
    uid
    title
    name
    hazard_class
    hazard_code
    quantity
    weight
  }
`;

export const TransportationFragment = gql`
  fragment Transportation on Transportation {
    id
    title
    comment
    date
    slug
  }
`;

export const VesselFragment = gql`
  fragment Vessel on Vessel {
    uid
    name
  }
`;

export const DocumentFragment = gql`
  fragment Document on Document {
    uid
    name
    path
    date
  }
`;

export const DocumentCategoryFragment = gql`
  fragment DocumentCategory on DocumentCategory {
    name
    documents {
      ...Document
    }
  }
  ${DocumentFragment}
`;
