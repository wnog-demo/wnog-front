import gql from 'graphql-tag';

export const NotificationFragment = gql`
  fragment Notification on Notification {
    id
    name
    slug
    value
  }
`;

export const UserRoleFragment = gql`
  fragment UserRole on UserRole {
    id
    name
    slug
  }
`;

export const ClientFragment = gql`
  fragment Client on Client {
    uid
    name
  }
`;

export const ContactFragment = gql`
  fragment Contact on Contact {
    uid
    name
  }
`;

export const UserFragment = gql`
  fragment User on User {
    uid
    role {
      ...UserRole
    }
    client {
      ...Client
    }
    name
    email
    gender
  }
  ${UserRoleFragment}
  ${ClientFragment}
`;
