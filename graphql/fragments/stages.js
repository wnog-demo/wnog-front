import gql from 'graphql-tag';

export const StageFragment = gql`
  fragment Stage on Stage {
    id
    title
    comment
    status
    date
  }
`;

export const StagesFragment = gql`
  fragment Stages on Stages {
    type
    number
    date
    list {
      ...Stage
    }
  }
  ${StageFragment}
`;
