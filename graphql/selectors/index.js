import { STAGES } from 'lib/constants';

export const getUserData = user => {
  if (!user) return {};

  const {
    uid,
    email,
    name,
    gender,
    role: { slug: role },
    client
  } = user;

  return {
    uid,
    email,
    name,
    gender,
    role,
    client
  };
};

export const getLastStages = stages => {
  stages = stages.reduce((list, item) => ({ ...list, [item.type]: item }), {});

  const result = Object.values(STAGES).reduce((acc, key) => {
    const array = stages[key];
    if (array && array.list.length > 0) {
      const last = { ...array.list[array.list.length - 1] };
      last.type = key;

      acc = [...acc, last];
    }
    return acc;
  }, []);

  return result;
};

export const getStagesArray = stages => {
  const result = Object.values(STAGES).reduce((acc, key) => {
    const array = stages[key];
    if (array && array.length > 0) {
      acc = [
        ...acc,
        ...array.map(el => {
          const item = { ...el };
          item.type = key;

          return item;
        })
      ];
    }
    return acc;
  }, []);
  return result;
};
