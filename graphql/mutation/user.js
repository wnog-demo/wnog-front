import gql from 'graphql-tag';

export const CHECK_SET_PASSWORD = gql`
  mutation($hash: String!, $email: String!) {
    checkSetPassword(hash: $hash, email: $email)
  }
`;

export const GET_TOKEN = gql`
  mutation($email: String!, $password: String!) {
    getToken(email: $email, password: $password)
  }
`;

export const SET_PASSWORD = gql`
  mutation($hash: String!, $email: String!, $password: String!, $repassword: String!) {
    setPassword(hash: $hash, email: $email, password: $password, repassword: $repassword)
  }
`;

export const CHANGE_PASSWORD = gql`
  mutation($password: String!, $newpassword: String!, $renewpassword: String!) {
    changePassword(password: $password, newpassword: $newpassword, renewpassword: $renewpassword)
  }
`;

export const RESTORE_PASSWORD = gql`
  mutation($email: String!) {
    restorePassword(email: $email)
  }
`;

export const IS_AUTH = gql`
  mutation {
    isAuth
  }
`;

export const UPDATE_NOTIFICATIONS = gql`
  mutation($values: String!) {
    updateNotifications(values: $values)
  }
`;

export const DOWNLOAD_DOCUMENTS = gql`
  mutation($uids: [String]) {
    downloadDocuments(uids: $uids)
  }
`;
