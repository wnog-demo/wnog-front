import gql from 'graphql-tag';
import {
  UserFragment,
  ClientFragment,
  CustomFragment,
  ShipperFragment,
  ProductFragment,
  StagesFragment,
  RouteFragment,
  ShipmentFragment,
  TransportationFragment,
  ContactFragment,
  PlaceFragment,
  VesselFragment,
  DocumentCategoryFragment
} from '../fragments';

export const GET_CUSTOM_CLEARANCE = gql`
  query(
    $uid: String
    $limit: Int
    $offset: Int
    $query: String
    $type: [String]
    $customs: [String]
    $ata: String
    $eta: String
    $greenlights: [String]
    $transportations: [String]
    $declarations: [String]
    $is_archive: Boolean
  ) {
    customClearance(
      uid: $uid
      limit: $limit
      offset: $offset
      query: $query
      type: $type
      customs: $customs
      ata: $ata
      eta: $eta
      greenlights: $greenlights
      transportations: $transportations
      declarations: $declarations
      is_archive: $is_archive
    ) {
      uid
      type
      date
      reference
      vessel {
        ...Vessel
      }
      user {
        ...User
      }
      contact {
        ...Contact
      }
      customer {
        ...Client
      }
      custom {
        ...Custom
      }
      shipper {
        ...Shipper
      }
      consignee {
        ...Shipper
      }
      product {
        ...Product
      }
      stages {
        ...Stages
      }
      transportations {
        ...Transportation
      }
      route {
        ...Route
      }
      shipments {
        ...Shipment
      }
      shipments {
        ...Shipment
      }
      delivery {
        ...Place
      }
      documents {
        ...DocumentCategory
      }
    }
  }
  ${UserFragment}
  ${ClientFragment}
  ${CustomFragment}
  ${ShipperFragment}
  ${ProductFragment}
  ${StagesFragment}
  ${RouteFragment}
  ${ShipmentFragment}
  ${TransportationFragment}
  ${ContactFragment}
  ${PlaceFragment}
  ${VesselFragment}
  ${DocumentCategoryFragment}
`;

export const GET_FILTER = gql`
  query {
    filter
  }
`;
