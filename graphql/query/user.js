import gql from 'graphql-tag';
import { UserFragment, NotificationFragment } from '../fragments';

export const GET_USER = gql`
  query($uid: String) {
    user(uid: $uid) {
      ...User
    }
  }
  ${UserFragment}
`;

export const GET_NOTIFICATIONS = gql`
  query {
    notification {
      ...Notification
    }
  }
  ${NotificationFragment}
`;
