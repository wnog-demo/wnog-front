import styled, { css } from 'styled-components';
import tools from 'lib/styles/tools';

const Checkbox = {};

Checkbox.Input = styled.div`
  display: flex;
  flex-shrink: 0;
  width: 36px;
  height: 20px;
  border-radius: 1rem;
  transition: all 0.2s ease;
  ${({ theme: { color } }) => css`
    background-color: ${color.lightgray};
  `}
`;

Checkbox.TickInput = styled.div`
  color: #2cabfc;
  border: 1.7px solid;
  border-radius: 3px;
  height: 17px;
  width: 17px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 10px;
`;

Checkbox.TickIcon = styled.div`
  display: flex;
  font-size: 7px;
  transition: all 0.3s ease;
  transform: scale(0.75);
  opacity: 0;
`;

Checkbox.Input.active = css`
  ${({ theme: { props, color } }) => css`
    ${props.view === 'blue' &&
      css`
        background-color: ${color.blue};
      `}
  `}
`;

Checkbox.Circle = styled.div`
  width: 12px;
  height: 12px;
  display: inline-flex;
  border-radius: 50%;
  margin: 4px 0 0 4px;
  transition: all 0.2s ease;
  ${({ theme: { color } }) => css`
    background-color: ${color.black};
  `}
`;

Checkbox.Content = styled.div`
  ${tools.flex}
`;

Checkbox.Circle.active = css`
  transform: translateX(15px);

  ${({ theme: { props, color } }) => css`
    ${props.view === 'blue' &&
      css`
        background-color: ${color.white};
      `}
  `}
`;

Checkbox.Label = styled.label`
  cursor: pointer;
  display: flex;
  align-items: center;
`;

Checkbox.Main = styled.div`
  user-select: none;
  cursor: pointer;
  display: inline-flex;
  align-items: center;
  ${({ theme: { props, color } }) => css`
    input {
      display: none;
      :checked + ${Checkbox.Input} {
        ${Checkbox.Circle} {
          ${Checkbox.Circle.active}
        }
        ${Checkbox.Input.active}
      }
      &:checked + ${Checkbox.TickInput} {
        ${Checkbox.TickIcon} {
          opacity: 1;
          transform: scale(1);
        }
      }
    }
    :hover {
      ${Checkbox.Circle} {
        background-color: ${props.isActive ? 'white' : color.blue};
      }
      ${Checkbox.Input} {
        background-color: ${color.lightgray} !important;
      }
    }
  `}
`;

export default Checkbox;
