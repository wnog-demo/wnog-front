import React, { Component } from 'react';
import { ThemeProvider } from 'styled-components';
import PropTypes from 'prop-types';

import { Icon } from 'elements';
import Block from './Checkbox.style';

export default class Checkbox extends React.PureComponent {
  static propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.bool,
    view: PropTypes.oneOf(['blue', 'tick'])
  };

  onChange = e => {
    const { onChange } = this.props;
    onChange && onChange();
  };

  render() {
    const { children, onChange, value, view, ...restProps } = this.props;
    const props = {
      view,
      isActive: value
    };

    return (
      <ThemeProvider theme={{ props }}>
        <Block.Main {...restProps}>
          <Block.Label>
            <input type="checkbox" onChange={this.onChange} checked={value} />
            {view !== 'tick' && (
              <Block.Input>
                <Block.Circle />
              </Block.Input>
            )}
            {view === 'tick' && (
              <Block.TickInput>
                <Block.TickIcon>
                  <Icon name={'tick'} />
                </Block.TickIcon>
              </Block.TickInput>
            )}
            {children && <Block.Content>{children}</Block.Content>}
          </Block.Label>
        </Block.Main>
      </ThemeProvider>
    );
  }
}
