import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';
import Block from './Text.style.js';

export default class Text extends React.PureComponent {
  constructor(props) {
    super(props);
  }
  static propTypes = {
    as: PropTypes.string,
    style: PropTypes.object,
    view: PropTypes.oneOf([
      'h1',
      'h1Left',
      'detailHeadline',
      'bold',
      'boldLh16',
      'opacityBold',
      'common',
      'common14',
      'note',
      'label',
      'upperSpacing',
      'opacity',
      'date',
      'navigationLink',
      'lang'
    ]),
    fontWeight: PropTypes.oneOf(['light', 'regular', 'bold', 'extrabold']),
    uppercase: PropTypes.bool,
    /* fontSize from global styles */
    fontSize: PropTypes.number,
    lineheight: PropTypes.number,
    opacity: PropTypes.number
  };
  static defaultProps = {
    as: 'div',
    children: '',
    style: {}
  };
  getProps() {
    const { view, uppercase, fontSize, lineheight, opacity, fontWeight } = this.props;

    return {
      view,
      uppercase,
      fontSize,
      lineheight,
      opacity,
      fontWeight
    };
  }
  render() {
    const { children, as, style } = this.props;
    const props = this.getProps();

    return (
      <ThemeProvider theme={{ props }}>
        <Block style={style} as={as}>
          {children}
        </Block>
      </ThemeProvider>
    );
  }
}
