import styled, { css } from 'styled-components';
import tools from 'lib/styles/tools';

export const views = {};

views['h1'] = css`
  ${({ theme }) => css`
    font-size: 36px;
    line-height: 1.5;
    text-align: center;
    letter-spacing: -0.01em;
    @media screen and (max-width: 768px) {
      font-size: 28px;
    }
    @media screen and (max-width: 450px) {
      font-size: 22px;
    }
  `}
`;

views['h1Left'] = css`
  ${({ theme }) => css`
    font-size: 36px;
    line-height: 46px;
    text-align: left;
    letter-spacing: -0.01em;
  `}
`;

views['detailHeadline'] = css`
  ${({ theme }) => css`
    font-size: 18px;
    line-height: 28px;
    text-align: left;
    letter-spacing: 0.01em;
  `}
`;

views['bold'] = css`
  ${({ theme }) => css`
    font-size: 12px;
    line-height: 1;
    opacity: 0.9;
    letter-spacing: 0.01em;
  `}
`;

views['boldLh16'] = css`
  ${({ theme }) => css`
    font-size: 12px;
    line-height: 16px;
    opacity: 0.9;
    letter-spacing: 0.01em;
  `}
`;

views['opacityBold'] = css`
  ${({ theme: { color } }) => css`
    font-size: 12px;
    line-height: 1;
    letter-spacing: 0.01em;
    font-weight: 700;
    opacity: 0.5;
  `}
`;

views['common'] = css`
  ${({ theme }) => css`
    text-align: center;
    font-size: 12px;
    line-height: 20px;
    font-weight: 400;
  `}
`;

views['common14'] = css`
  ${({ theme }) => css`
    text-align: center;
    font-size: 14px;
    line-height: 22px;
    font-weight: 400;
  `}
`;

views['note'] = css`
  ${({ theme }) => css`
    font-size: 12px;
    opacity: 0.5;
    font-weight: 400;
  `}
`;

views['label'] = css`
  ${({ theme }) => css`
    text-transform: uppercase;
    letter-spacing: 0.08em;
    font-size: 9px;
    line-height: 1;
    font-weight: 600;
    color: inherit;
  `}
`;

views['upperSpacing'] = css`
  ${({ theme }) => css`
    letter-spacing: 0.11em;
    text-transform: uppercase;
    font-size: 9px;
    line-height: 12px;
    opacity: 0.9;
    font-weight: 700;
  `}
`;

views['opacity'] = css`
  ${({ theme }) => css`
    letter-spacing: 0.11em;
    text-transform: uppercase;
    font-size: 9px;
    line-height: 12px;
    font-weight: 400;
    opacity: 0.5;
  `}
`;

views['date'] = css`
  ${({ theme }) => css`
    letter-spacing: 0.08em;
    text-transform: uppercase;
    font-size: 9px;
    line-height: 12px;
    opacity: 0.45;
  `}
`;

views['navigationLink'] = css`
  ${({ theme }) => css`
    font-size: 13px;
    line-height: 16px;
    letter-spacing: 0.03em;
    opacity: 0.9;
  `}
`;

views['lang'] = css`
  ${({ theme }) => css`
    font-size: 11px;
    line-height: 12px;
    letter-spacing: 0.1em;
    opacity: 0.9;
    text-transform: uppercase;
  `}
`;

const Text = styled.div`
  font-weight: bold;
  font-size: 1em;
  text-transform: inherit;
  ${({ theme: { props, fontSize, fontWeight, lineHeight } }) => {
    const view = props.view && views[props.view];

    return css`
      ${view}
      ${props.uppercase &&
        css`
          text-transform: uppercase;
        `}
      ${props.fontSize &&
        (fontSize[props.fontSize]
          ? fontSize[props.fontSize]
          : css`
              font-size: ${props.fontSize}px;
            `)}
      ${props.lineHeight &&
        (lineHeight[props.lineHeight]
          ? lineHeight[props.lineHeight]
          : css`
              line-height: ${props.fontSize}px;
            `)}
      ${props.fontWeight &&
        (fontWeight[props.fontWeight]
          ? fontWeight[props.fontWeight]
          : css`
              font-weight: ${props.fontWeight};
            `)}
      ${props.opacity &&
        css`
          opacity: ${[props.opacity]};
        `}
    `;
  }}
`;

export default Text;
