import styled, { css } from 'styled-components';
import tools from 'lib/styles/tools';

const Scrollbar = styled.div`
 
`;

Scrollbar.Thumb = styled.div`
  background-color: rgba(255,255,255,0.15);
  border-radius: 3px;
`;

export default Scrollbar;
