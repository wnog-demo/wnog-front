import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Block from './Scrollbar.style';
import { Scrollbars } from 'react-custom-scrollbars';

export default class Scrollbar extends React.PureComponent {
  static propTypes = {
    children: PropTypes.any.isRequired,
    getRef: PropTypes.func
  };
  constructor(props) {
    super(props);

    this.scrollBlock = React.createRef();
  }
  renderThumb() {
    return <Block.Thumb />;
  }
  render() {
    return (
      <Scrollbars
        ref={this.props.getRef}
        universal
        renderThumbHorizontal={this.renderThumb}
        renderThumbVertical={this.renderThumb}
      >
        {this.props.children}
      </Scrollbars>
    );
  }
}
