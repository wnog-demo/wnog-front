import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actionsCreators from 'store/actions';
import { ThemeProvider } from 'styled-components';
import { Icon } from 'elements';
import Block from './Notification.style';

const mapStateToProps = state => {
  return {
    notification: state.notification
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      showNotification: bindActionCreators(actionsCreators.showNotification, dispatch),
      changeNotificationState: bindActionCreators(actionsCreators.changeNotificationState, dispatch)
    }
  };
};

@connect(
  mapStateToProps,
  mapDispatchToProps
)
export default class Notification extends React.PureComponent {
  static propTypes = {
    type: PropTypes.oneOf(['error'])
  };
  timer = null;
  componentDidUpdate() {
    const { notification, actions } = this.props;

    clearTimeout(this.timer);
    if (notification.active) {
      this.timer = setTimeout(e => {
        actions.changeNotificationState(false);
      }, 7000);
    }
  }
  componentWillUnmount() {
    clearTimeout(this.timer);
  }
  onClose = e => {
    const { actions } = this.props;

    actions.changeNotificationState(false);
  };
  render() {
    const {
      notification: { active, html, type }
    } = this.props;
    const props = {
      active,
      type
    };

    return (
      <ThemeProvider theme={{ props }}>
        <Block.Root>
          <Block.Wrapper>
            <div dangerouslySetInnerHTML={{ __html: html }} />
            <Block.Close onClick={this.onClose}>
              <Icon name="cross" />
            </Block.Close>
          </Block.Wrapper>
        </Block.Root>
      </ThemeProvider>
    );
  }
}
