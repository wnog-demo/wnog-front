import styled, { css } from 'styled-components';

const Notification = {};

Notification.Root = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 0;
`;

Notification.Wrapper = styled.div`
  width: 100%;
  color: white;
  text-align: center;
  font-size: 11px;
  letter-spacing: 0.12em;
  text-transform: uppercase;
  padding: 2rem 3rem;
  position: relative;
  transition: transform 0.2s ease, visibility 0s ease 0.2s;
  visibility: hidden;
  ${({ theme: { props, color } }) => css`
    background-color: ${color.blue};
    ${props.active &&
      css`
        transform: translateY(-100%);
        visibility: visible;
        transition: transform 0.2s ease;
      `}
    ${props.type === 'error' &&
      css`
        background-color: ${color.red};
      `}
  `}
`;

Notification.Close = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  position: absolute;
  top: 50%;
  right: 2rem;
  transform: translateY(-50%);
`;

export default Notification;
