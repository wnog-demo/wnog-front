import styled, { css } from 'styled-components';
import tools from 'lib/styles/tools';

const views = {};

views['common'] = css`
  ${({ theme: { color } }) => css`
    min-width: 148px;
    height: auto;
    font-size: 11px;
    line-height: 12px;
    letter-spacing: 0.12em;
    padding: 21px 50px;
    color: ${color.white};

    text-transform: uppercase;
    background-color: ${color.blue};
    transition: all 0.2s ease;
    :hover,
    :focus {
      background-color: ${color.black};
      opacity: 0.85;
    }
  `}
`;

views['common-new'] = css`
  ${({ theme: { color } }) => css`
    ${views['common']}
    background-color: #3c7cb5;
  `}
`;

views['large'] = css`
  ${({ theme: { color } }) => css`
    min-width: 148px;
    height: auto;
    font-size: 11px;
    line-height: 12px;
    letter-spacing: 0.12em;
    padding: 21px 40px;
    color: ${color.white};

    text-transform: uppercase;
    background-color: ${color.blue};
    transition: all 0.2s ease;
    :hover,
    :focus {
      background-color: ${color.black};
      opacity: 0.85;
    }
  `}
`;

const Button = styled.button`
  cursor: pointer;
  -webkit-appearance: none;
  outline: none;
  border: none;
  font-weight: 600;
  font-size: 1em;
  color: currentColor;
  user-select: none;
  text-transform: inherit;
  letter-spacing: inherit;
  height: fit-content;
  transition: all 0.1s ease;

  ${({ theme: { props, fontSize, color } }) => css`
      ${props.view && views[props.view] ? views[props.view] : ''}
      ${props.view === 'tag' && props.active && views['tag'].active}
      ${props.fit &&
        css`
          height: 100%;
          width: 100%;
        `}
      ${props.fontSize && fontSize[props.fontSize]}
      ${props.disabled &&
        css`
          opacity: 0.35;
          pointer-events: none;
        `}
  `}
`;

export default Button;
