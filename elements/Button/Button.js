import React, { Component } from 'react';
import Block from './Button.style.js';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';

export default class Button extends React.PureComponent {
  static propTypes = {
    as: PropTypes.string,
    style: PropTypes.object,
    onClick: PropTypes.func,
    /*  <button type="button" /> */
    type: PropTypes.string,
    /*  style views /> */
    view: PropTypes.oneOf(['common', 'common-new', 'large']),
    /*  state active /> */
    active: PropTypes.bool,
    /*  state disabled /> */
    disabled: PropTypes.bool,
    /*  style fit wrapper /> */
    fit: PropTypes.bool,
    fontSize: PropTypes.number,
    tabIndex: PropTypes.number
  };
  static defaultProps = {
    type: 'button',
    as: 'button',
    children: '',
    style: {}
  };
  getProps = e => {
    const { view, state, disabled, fit, fontSize, active } = this.props;

    return {
      view,
      state,
      disabled,
      fit,
      fontSize,
      active
    };
  };
  render() {
    const { children, as, onClick, href, type, style, tabIndex } = this.props;
    const props = this.getProps();

    return (
      <ThemeProvider theme={{ props }}>
        <Block
          tabIndex={tabIndex}
          style={style}
          as={as}
          href={href}
          type={type}
          onClick={onClick}
          index={0}
        >
          {children}
        </Block>
      </ThemeProvider>
    );
  }
}
