import React, { Component } from 'react';
import { Icon } from 'elements';
import MomentLocaleUtils from 'react-day-picker/moment';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'moment/locale/ru';

import './DayPicker.css';

/**
 * Элемент управления датами
 */
export default class DatePicker extends React.PureComponent {
  static defaultProps = {
    from: undefined,
    to: undefined,
    onChange: () => {},
    locale: 'ru'
  };

  handleDayClick(day) {
    const range = DateUtils.addDayToRange(day, this.props);

    this.props.onChange(range);
  }

  render() {
    const { from, to, locale } = this.props;
    const modifiers = { start: from, end: to };

    return (
      <DayPicker
        selectedDays={[from, { from, to }]}
        modifiers={modifiers}
        onDayClick={day => this.handleDayClick(day)}
        localeUtils={MomentLocaleUtils}
        locale={locale}
        renderDay={(day, modifiers) => <div>{day.getDate()}</div>}
      />
    );
  }
}
