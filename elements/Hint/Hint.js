import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Block from './Hint.style';

export default class Hint extends React.PureComponent {
  static propTypes = {
    /** Содержимое */
    children: PropTypes.node.isRequired,
    /** Лейбл */
    label: PropTypes.node.isRequired
  };

  render() {
    const { children, label } = this.props;

    return (
      <Block>
        <Block.Window>{children}</Block.Window>
        {label}
      </Block>
    );
  }
}
