import styled from 'styled-components';

const Block = styled.div`
  position: relative;
  display: inline-block;
`;

Block.Window = styled.div`
  position: absolute;
  bottom: calc(100% + 10px);
  left: 50%;
  background: #ffffff;
  box-shadow: 0 3px 15px rgba(0, 0, 0, 0.1);
  padding: 12px 20px;
  transform: translateX(-50%);
  white-space: nowrap;
  border-radius: 5px;
  line-height: 1;
  transition: all 0.2s ease;
  color: black;
  z-index: 5;

  ${Block}:not(:hover) & {
    opacity: 0;
    transform: translateY(-5px) translateX(-50%);
    pointer-events: none;
    transition: none;
  }
  &:before {
    content: '';
    display: block;
    border: 8px solid transparent;
    border-top: 8px solid #ffffff;
    position: absolute;
    top: 100%;
    left: calc(50% - 4px);
  }
`;

export default Block;
