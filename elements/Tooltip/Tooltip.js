import React, { Component } from 'react';
import Block from './Tooltip.style';

export default class Tooltip extends React.PureComponent {
  static defaultProps = {
    children: ''
  };
  render() {
    return (
      <Block.Root>
        <Block.Tooltip>
          <Block.Content>
            <Block.ContentWrapper>{this.props.children}</Block.ContentWrapper>
          </Block.Content>
        </Block.Tooltip>
      </Block.Root>
    );
  }
}
