import styled, { css } from 'styled-components';

const Loader = {};

Loader.Root = styled.div`
  display: inline-flex;
`;

Loader.Dot = styled.div`
  transform: scale(0);
  @keyframes anim {
    0% {
      transform: translateY(0) scale(0);
    }
    25% {
      transform: translateY(5px) scale(0.5);
    }
    50% {
      transform: translateY(0) scale(1);
    }
    75% {
      transform: translateY(-5px) scale(0.5);
    }
    100% {
      transform: translateY(0) scale(0);
    }
  }

  ${({ theme: { color } }) => css`
    display: flex;
    height: 0.75rem;
    width: 0.75rem;
    background-color: ${color.blue};
    border-radius: 50%;
    animation: anim 0.8s linear infinite;
    :not(:last-child) {
      margin-right: 0.25rem;
    }
    :nth-child(2) {
      animation: anim 0.8s linear 0.4s infinite;
    }
    :nth-child(3) {
      animation: anim 0.8s linear infinite;
    }
  `}
`;

export default Loader;
