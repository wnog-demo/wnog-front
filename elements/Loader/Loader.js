import React, { Component } from 'react';

import Block from './Loader.style';

export default class Loader extends React.PureComponent {
  render() {
    return (
      <Block.Root>
        <Block.Dot />
        <Block.Dot />
        <Block.Dot />
      </Block.Root>
    );
  }
}
