import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Block from './Icon.style';
import * as icons from './static';

const iconNames = Object.keys(icons);

export default class Icon extends React.PureComponent {
  static propTypes = {
    /** Name of svg file */
    name: PropTypes.oneOf(iconNames),
    fill: PropTypes.string,
    rotate: PropTypes.number
  };
  render() {
    const { name, fill, rotate } = this.props;
    const SvgIcon = icons[name];

    return <Block transform={rotate ? `rotate(${rotate})` : null} fill={fill} as={SvgIcon} />;
  }
}
