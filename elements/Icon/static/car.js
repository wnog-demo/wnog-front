import React from 'react';

export default props => (
  <svg width="25" height="18" viewBox="0 0 25 18" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path
      clipRule="evenodd"
      d="M17.5 15c0 1.104.896 2 2 2 1.106 0 2-.896 2-2s-.894-2-2-2c-1.104 0-2 .896-2 2zM3.5 15c0 1.104.896 2 2 2 1.106 0 2-.896 2-2s-.894-2-2-2c-1.104 0-2 .896-2 2z"
      stroke="#2CABFC"
      fill="none"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M21.5 15H24V9.5l-2-2L19.5 3H15V1H1v14h2.5M17.5 15h-10"
      stroke="#2CABFC"
      fill="none"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M17 5.5h1.5l1.5 3M15 3v11M24 12H1"
      stroke="currentColor"
      fill="none"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
