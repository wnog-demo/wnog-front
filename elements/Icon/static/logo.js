import React from 'react';

export default props => (
  <svg
    width="0.5em"
    height="1em"
    viewBox="0 0 94 179"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path d="M67.2515 90.7172L0 137.501L17.5439 73.1733L67.2515 90.7172Z" fill="#2CABFC" />
    <path d="M64.3243 81.9466L23.3887 67.3267L55.5524 46.8589L64.3243 81.9466Z" fill="#2CABFC" />
    <path
      d="M40.9346 0.0776367L52.6305 38.0893L20.4668 58.5572L40.9346 0.0776367Z"
      fill="#D5D9E0"
    />
    <path d="M70.179 99.4897L5.85156 143.349L93.5709 178.437L70.179 99.4897Z" fill="#D51223" />
  </svg>
);
