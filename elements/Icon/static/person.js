import React from 'react';

export default props => (
  <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M15 14c0-2.5909-1.4077-4.85314-3.5-6.06348M1 14c0-2.5909 1.4077-4.85314 3.5-6.06348"
      stroke="currentColor"
      strokeWidth="2"
    />
    <circle cx="8" cy="5" r="4" stroke="currentColor" strokeWidth="2" />
  </svg>
);
