import React from 'react';

export default props => (
  <svg width="27" height="19" viewBox="0 0 27 19" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path
      clipRule="evenodd"
      d="M23.8261 7.13046H7.52174L5.34783 4.95654H1l3.26087 6.52176h8.69563L6.43478 18h5.43482l6.5217-6.5217h5.4348c1.2011 0 2.1739-.9739 2.1739-2.17393 0-1.2-.9728-2.17391-2.1739-2.17391z"
      stroke="currentColor"
      fill="none"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M18.392 7.13046l-5.4348-5.43478H8.60938l5.43482 5.43478"
      stroke="currentColor"
      fill="none"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
