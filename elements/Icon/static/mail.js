import React from 'react';

export default props => (
  <svg
    width="1em"
    height="0.75em"
    viewBox="0 0 16 12"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M2 0C0.89543 0 0 0.895431 0 2V10C0 11.1046 0.895431 12 2 12H14C15.1046 12 16 11.1046 16 10V2C16 0.89543 15.1046 0 14 0H2ZM12.484 2L3.51598 2L7.32173 5.51299C7.70479 5.86659 8.29522 5.86659 8.67829 5.51299L12.484 2ZM2 3.32245V10H14V3.32246L10.0348 6.9826C8.88566 8.04338 7.11435 8.04339 5.96517 6.9826L2 3.32245Z"
    />
  </svg>
);
