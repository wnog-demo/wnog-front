import React from 'react';

export default props => (
  <svg
    width="25"
    height="18"
    viewBox="0 0 25 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M23.8848 15.5l-1.885-1.5h-2.281"
      stroke="currentColor"
      fill="none"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      clipRule="evenodd"
      d="M16 15c0 1.104.896 2 2 2 1.103 0 2-.896 2-2s-.897-2-2-2c-1.104 0-2 .896-2 2zm-6 0c0 1.104.896 2 2 2 1.103 0 2-.896 2-2s-.897-2-2-2c-1.104 0-2 .896-2 2zm-6 0c0 1.104.896 2 2 2 1.103 0 2-.896 2-2s-.897-2-2-2c-1.104 0-2 .896-2 2z"
      stroke="currentColor"
      fill="none"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13 1H1m11 0v6h9.5c1.5 0 1.5 7 0 7M3 1v7H1v6M21 3h-4"
      stroke="currentColor"
      fill="none"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      clipRule="evenodd"
      d="M5.5 7h4V3.5h-4V7z"
      stroke="currentColor"
      fill="none"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M16.2812 14h-2.5469m-3.4687 0H7.71863m-3.43738 0h-3.282"
      stroke="currentColor"
      fill="none"
      strokeWidth="1.5"
      strokeLinejoin="round"
    />
    <path
      d="M20 3v4m-2-4v4"
      stroke="currentColor"
      fill="none"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
