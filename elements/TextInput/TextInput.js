import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';
import Block from './TextInput.style';
import { Icon } from 'elements';

export default class TextInput extends React.PureComponent {
  static propTypes = {
    /* css no padding */
    noPadding: PropTypes.bool,
    type: PropTypes.oneOf(['text', 'search', 'email', 'number', 'tel', 'password']),
    value: PropTypes.any,
    name: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
    onAnyEvent: PropTypes.func,
    placeholder: PropTypes.string,
    view: PropTypes.string,
    fit: PropTypes.bool,
    tabIndex: PropTypes.number,
    icon: PropTypes.any,
    maxLength: PropTypes.number,
    invalid: PropTypes.bool,
    errorMessage: PropTypes.any
  };
  static defaultProps = {
    type: 'text',
    view: 'default',
    maxLength: 64,
    value: ''
  };
  onChange(e) {
    const { onChange } = this.props;

    this.onAnyEvent('change', e);
    onChange && onChange(e.target.value);
  }
  onBlur(e) {
    const { onBlur } = this.props;

    this.onAnyEvent('blur', e);
    onBlur && onBlur();
  }
  onFocus(e) {
    const { onFocus } = this.props;

    this.onAnyEvent('focus', e);
    onFocus && onFocus();
  }
  onAnyEvent(type, e) {
    const { onAnyEvent, name } = this.props;

    onAnyEvent && onAnyEvent(type, name, e);
  }

  getProps() {
    const { view, fit, noPadding, invalid, icon } = this.props;

    return { view, fit, noPadding, invalid, icon };
  }
  render() {
    const {
      name,
      type,
      label,
      value,
      placeholder,
      tabIndex,
      icon,
      view,
      maxLength,
      errorMessage
    } = this.props;
    const props = this.getProps();

    const inputProps = {
      name,
      type,
      required: true,
      placeholder,
      tabIndex,
      value,
      maxLength,
      onChange: e => this.onChange(e),
      onBlur: e => this.onBlur(e),
      onFocus: e => this.onFocus(e)
    };

    return (
      <ThemeProvider theme={{ props }}>
        <Block.Root>
          <Block.Label>
            {label && <Block.LabelText>{label}</Block.LabelText>}
            <Block.InputWrapper>
              <Block.Input {...inputProps} />
              {icon && (
                <Block.InputIcon icon={icon}>
                  <Icon name={icon} />
                </Block.InputIcon>
              )}
            </Block.InputWrapper>
            {errorMessage && <Block.ErrorMessage>{errorMessage}</Block.ErrorMessage>}
          </Block.Label>
        </Block.Root>
      </ThemeProvider>
    );
  }
}
