import styled, { css } from 'styled-components';
import tools from 'lib/styles/tools';

const TextInput = {};

TextInput.Label = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
  ${({ theme: { props, color } }) => css`
    ${props.fit &&
      css`
        height: 100%;
      `}
  `}
`;

TextInput.LabelText = styled.div`
  text-transform: uppercase;
  font-weight: bold;
  padding-bottom: 0.75rem;
  flex-shrink: 0;
  ${({ theme: { props } }) => css`
    font-size: 11px;
    letter-spacing: 1;
  `}
`;

TextInput.Input = styled.input`
  width: 100%;
  border: 2px solid transparent;
  padding: 19px 15px 19px 15px;
  font-size: 12px;
  line-height: 1;
  opacity: 1 !important;

  ${({ theme: { props, color } }) => css`
    background-color: ${color.white};
    &::placeholder {
      color: ${color.black};
      opacity: 0.3;
      font-size: 12px;
      line-height: 1;
    }
    :focus {
      border-color: ${color.lightblue};
      caret-color: ${color.blue};
    }
    ${props.invalid &&
      css`
        color: ${color.red};
      `}
    ${props.view === 'search' &&
      css`
        padding: 16px 15px 16px 44px;
      `}
  `}
`;

TextInput.InputIcon = styled.div`
  position: absolute;
  top: calc(50% - 6px);
  right: 20px;
  font-size: 16px;
  line-height: 0.75rem;
  pointer-events: none;
  ${({ icon, theme: { props, color } }) => css`
    color: ${color.blue};

    ${icon === 'search' &&
      css`
        top: calc(50% - 7px);
        font-size: 15px;
        left: 16px;
        color: ${color.black};
      `}

    ${icon === 'eyeOpen' &&
      css`
        top: calc(50% - 7px);
        font-size: 22px;
        ${TextInput.Input} {
          padding-right: 30px;
        }
      `}

    ${icon === 'eyeClosed' &&
      css`
        top: calc(50% - 7px);
        font-size: 22px;
      `}

  `}
`;

TextInput.InputWrapper = styled.div`
  position: relative;
`;

TextInput.Root = styled.div`
  ${tools.flex}
  text-align: left;
  width: 100%;
  ${({ theme: { props, color } }) => css`
    ${props.fit &&
      css`
        height: 100%;
      `}
  `}
`;

TextInput.ErrorMessage = styled.div`
  ${tools.flex}
  padding-top: 0.5rem;
  font-size: 12px;
  ${({ theme: { props, color } }) => css`
    color: ${color.red};
  `}
`;

export default TextInput;
