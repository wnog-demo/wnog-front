import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql, compose, withApollo } from 'react-apollo';

import { CHANGE_PASSWORD } from 'graphql/mutation';
import actionsCreators from 'store/actions';
import { Text, Icon, TextInput, Button, Checkbox } from 'elements';
import { Validation } from 'lib/utils';
import Block from './ChangePassword.style';
import { i18n } from 'lib/constants';

const field = {
  value: '',
  isValid: false,
  showErrors: false,
  errorMessage: ''
};

const validationOptions = {
  password: {
    options: {
      required: true,
      minLength: 8
    }
  },
  newpassword: {
    options: {
      required: true,
      mediumPassword: true
    }
  },
  renewpassword: {
    options: {
      required: true,
      mediumPassword: true
    }
  }
};

const mapStateToProps = state => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      showNotification: bindActionCreators(actionsCreators.showNotification, dispatch)
    }
  };
};

@connect(
  mapStateToProps,
  mapDispatchToProps
)
@withApollo
export default class ChangePassword extends React.PureComponent {
  isSent = false;
  state = {
    isSent: false,
    passwordsAreEqual: false,
    form: { showErrors: false },
    fields: {
      password: { ...field },
      newpassword: { ...field },
      renewpassword: { ...field }
    }
  };

  get isValid() {
    const { fields } = this.state;

    return !Object.keys(validationOptions).some(name => {
      return !fields[name].isValid;
    });
  }

  componentDidMount() {
    const {
      app: { lang }
    } = this.props;
    this.validation = new Validation({ config: validationOptions, lang });
  }

  onInputChange(name, value) {
    const fields = this.validation.onInputChange(this.state.fields, name, value);

    this.setState({ fields }, e => {
      if (['newpassword', 'renewpassword'].includes(name)) {
        const isEqual =
          this.state.fields.newpassword.value === this.state.fields.renewpassword.value;
        if (isEqual !== this.state.passwordsAreEqual) {
          this.setState({
            passwordsAreEqual: isEqual
          });
        }
      }
    });
  }

  onInputFocus(name) {
    this.changeErrorVisibility(name, false);
  }

  onInputBlur(name) {
    this.changeErrorVisibility(name, true);
  }

  onAnyInputEvent(type, name, e) {
    if (type === 'change') name && this.onInputChange(name, e.target.value);
    if (type === 'focus') name && this.onInputFocus(name);
    if (type === 'blur') name && this.onInputBlur(name);
  }

  validate(name, value) {
    const valid = this.validation.validate(value, name);

    return valid;
  }

  onErrorMessageClose = name => {
    this.changeErrorVisibility(name, false);
  };

  changeErrorVisibility(name, bool) {
    const { fields } = this.state;
    const field = fields[name];

    this.setState({
      fields: {
        ...fields,
        [name]: {
          ...field,
          showErrors: bool
        }
      }
    });
  }

  isVisibleError(name) {
    const { fields, form } = this.state;
    const field = fields[name];

    return !field.isValid && (form.showErrors || field.showErrors);
  }

  onDisabledButtonClick = e => {
    const {
      actions,
      app: { lang }
    } = this.props;

    const fields = this.validation.validateForm(this.state.fields);

    this.setState({
      fields,
      form: { showErrors: true }
    });

    if (!this.isValid) {
      actions.showNotification(i18n('fields_require_password', lang), 'error');
      return false;
    }
    if (!this.state.passwordsAreEqual) {
      actions.showNotification(i18n('passwords_do_not_match', lang), 'error');
      return false;
    }
  };

  resetForm() {
    this.setState({
      fields: {
        password: { ...field },
        newpassword: { ...field },
        renewpassword: { ...field }
      }
    });
  }

  onSubmit = e => {
    const {
      actions,
      client,
      app: { lang }
    } = this.props;
    const {
      fields: { password, newpassword, renewpassword }
    } = this.state;

    e.preventDefault();

    if (this.isSent) return false;

    if (!(this.isValid && this.state.passwordsAreEqual)) {
      this.onDisabledButtonClick();
      return false;
    }

    this.isSent = true;
    this.setState({
      isSent: true
    });

    client
      .mutate({
        mutation: CHANGE_PASSWORD,
        variables: {
          password: password.value.trim(),
          newpassword: newpassword.value.trim(),
          renewpassword: renewpassword.value.trim()
        }
      })
      .then(res => {
        if (res.data && res.data.changePassword) {
          actions.showNotification(i18n('change_password_success', lang));
        } else {
          actions.showNotification(i18n('change_password_error', lang), 'error');
        }
        this.resetForm();
      });
  };

  render() {
    const {
      fields: { password, newpassword, renewpassword }
    } = this.state;

    const {
      app: { lang }
    } = this.props;

    const submitIsDisabled = !this.isValid || !this.state.passwordsAreEqual;

    return (
      <Block.Root>
        <Block.PasswordWrapper noValidate as={'form'} onSubmit={this.onSubmit}>
          <Block.Title>
            <Text view={'upperSpacing'}>{i18n('change_password', lang)}</Text>
          </Block.Title>
          <Block.InputWrapper>
            <Block.InputItem>
              <TextInput
                name={'password'}
                value={password.value}
                invalid={this.isVisibleError('password')}
                errorMessage={this.isVisibleError('password') && password.errorMessage}
                onAnyEvent={this.onAnyInputEvent.bind(this)}
                placeholder={i18n('current_password', lang)}
                type={'password'}
              />
            </Block.InputItem>
            <Block.InputItem>
              <TextInput
                name={'newpassword'}
                value={newpassword.value}
                invalid={this.isVisibleError('newpassword')}
                errorMessage={this.isVisibleError('newpassword') && newpassword.errorMessage}
                onAnyEvent={this.onAnyInputEvent.bind(this)}
                placeholder={i18n('new_password', lang)}
                type={'password'}
              />
            </Block.InputItem>
            <Block.InputItem>
              <TextInput
                name={'renewpassword'}
                value={renewpassword.value}
                invalid={this.isVisibleError('renewpassword')}
                errorMessage={this.isVisibleError('renewpassword') && renewpassword.errorMessage}
                onAnyEvent={this.onAnyInputEvent.bind(this)}
                placeholder={i18n('repeat_password', lang)}
                type={'password'}
              />
            </Block.InputItem>
            <Block.ButtonItem>
              {submitIsDisabled && <Block.DisabledButton onClick={this.onDisabledButtonClick} />}
              <Button
                type={'submit'}
                onClick={this.onSubmit}
                disabled={submitIsDisabled}
                view="common"
              >
                {i18n('save', lang)}
              </Button>
            </Block.ButtonItem>
          </Block.InputWrapper>
        </Block.PasswordWrapper>
      </Block.Root>
    );
  }
}
