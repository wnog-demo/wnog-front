import styled, { css } from 'styled-components';

const ChangePassword = {};

ChangePassword.Root = styled.div`
  width: 100%;
`;

ChangePassword.Title = styled.div`
  opacity: 0.6;
  margin-bottom: 2rem;
`;

ChangePassword.PasswordWrapper = styled.div`
  width: 100%;
  max-width: 1100px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  padding: 6rem 3rem 6.25rem 10.5rem;
`;

ChangePassword.InputWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
`;

ChangePassword.InputItem = styled.div`
  display: flex;
  margin-right: 20px;
  flex-basis: 350px;
`;

ChangePassword.ButtonItem = styled.div`
  display: flex;
  flex-shrink: 0;
  position: relative;
`;

ChangePassword.DisabledButton = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

export default ChangePassword;
