//base
export { default as Login } from './Login/Login';

//detail page
export { default as DetailHeader } from './DetailHeader/DetailHeader';
export { default as DetailSidebar } from './DetailSidebar/DetailSidebar';
export { default as DetailStatuses } from './DetailStatuses/DetailStatuses';
export { default as Documents } from './Documents/Documents';

//to page
export { default as StatusHistory } from './StatusHistory/StatusHistory';
export { default as StatusSidebar } from './StatusSidebar/StatusSidebar';

//settings
export { default as ChangePassword } from './ChangePassword/ChangePassword';

//reset page
export { default as PasswordReset } from './PasswordReset/PasswordReset';
