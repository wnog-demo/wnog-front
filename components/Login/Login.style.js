import styled, { css } from 'styled-components';

const Login = {};

Login.Root = styled.div`
  min-height: calc(100vh - 170px);
  position: relative;
  overflow: hidden;
  display: flex;
  align-items: center;
`;

Login.Wrapper = styled.div`
  padding: 4rem 1.5rem 4rem 1.5rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 47rem;
  max-width: 100%;
  margin: 0 auto;
  transform: translateX(-20%);
  @media screen and (max-width: 1024px) {
    transform: none;
  }
  @media screen and (max-width: 600px) {
    padding: 3rem 1rem 0 1rem;
  }
`;

Login.Headline = styled.div`
  margin-bottom: 1rem;
  width: 100%;
  display: flex;
  align-items: center;
`;

Login.HeadLineQuestion = styled.div`
  height: 85px;
  width: 85px;
  background-image: url('/static/images/ny-question.jpg');
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  margin-right: 10px;
  flex-shrink: 0;
`;

Login.HeadLineText = styled.div`
  max-width: 280px;
  font-weight: 500;
  ${({ theme: { color } }) => css`
    color: #3c7cb5;
  `}
`;

Login.ContentWrapper = styled.div`
  margin: 0 -10px;
  width: 100%;
`;

Login.Inputs = styled.div`
  flex: 1;
  display: flex;
  @media screen and (max-width: 600px) {
    margin: 0 -0.5rem 15px -0.5rem;
  }
  @media screen and (max-width: 450px) {
    display: block;
    margin-bottom: 0;
  }
`;

Login.InputWrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  padding-bottom: 1.5rem;
  @media screen and (max-width: 600px) {
    display: block;
  }
`;

Login.InputItem = styled.div`
  flex: 1;
  padding-right: 15px;
  @media screen and (max-width: 600px) {
    flex: auto;
    width: 50%;
    padding: 0 0.5rem;
  }
  @media screen and (max-width: 450px) {
    width: 100%;
    padding-bottom: 1rem;
  }
`;

Login.ButtonItem = styled.div`
  display: flex;
  flex-shrink: 0;
  position: relative;
  flex-shrink: 0;
`;

Login.DisabledButton = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
`;

Login.ForgotPassword = styled.button`
  ${({ theme: { color } }) => css`
    > div {
      transition: all 0.2s ease;
      color: ${color.blue};
    }
    :hover,
    :focus {
      > div {
        color: ${color.black};
      }
    }
  `}
`;

Login.NYImageWrapper = styled.div`
  position: absolute;
  top: 50%;
  transform: translateY(calc(-50% + 32px));
  width: 100%;
  right: calc(-50% - 15rem);
  img {
    height: 280px;
    position: relative;
  }
  &:after {
    content: '';
    position: absolute;
    left: 350px;
    right: 0;
    height: 100%;
    background-color: #c62329;
  }
  @media screen and (max-width: 1024px) {
    display: none;
  }
`;

export default Login;
