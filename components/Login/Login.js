import React, { Component } from 'react';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actionsCreators from 'store/actions';
import { graphql, compose, withApollo } from 'react-apollo';

import { Text, Icon, TextInput, Button } from 'elements';
import { Content } from 'containers';
import { Validation, signIn, logout } from 'lib/utils';
import { GET_TOKEN } from 'graphql/mutation';
import Block from './Login.style';
import { Link } from 'lib/routes';
import { i18n } from 'lib/constants';

const field = {
  value: '',
  isValid: false,
  showErrors: false,
  errorMessage: ''
};

const validationOptions = {
  email: {
    options: {
      required: true,
      email: true
    }
  },
  password: {
    options: {
      required: true,
      minLength: 8
    }
  }
};

const mapStateToProps = state => {
  return {
    app: state.app,
    notification: state.notification
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      showNotification: bindActionCreators(actionsCreators.showNotification, dispatch),
      changeToken: bindActionCreators(actionsCreators.changeToken, dispatch)
    }
  };
};

@withApollo
@withRouter
@connect(
  mapStateToProps,
  mapDispatchToProps
)
export default class Login extends React.PureComponent {
  isSent = false;

  state = {
    isSent: false,
    form: { showErrors: false },
    fields: {
      email: { ...field },
      password: { ...field }
    }
  };

  get isValid() {
    const { fields } = this.state;

    return !Object.keys(validationOptions).some(name => {
      return !fields[name].isValid;
    });
  }

  componentDidMount() {
    const {
      app: { lang }
    } = this.props;
    this.validation = new Validation({ config: validationOptions, lang });
  }

  onInputChange(name, value) {
    const fields = this.validation.onInputChange(this.state.fields, name, value);

    this.setState({ fields });
  }

  onInputFocus(name) {
    this.changeErrorVisibility(name, false);
  }

  onInputBlur(name) {
    this.changeErrorVisibility(name, true);
  }

  onAnyInputEvent(type, name, e) {
    if (type === 'change') name && this.onInputChange(name, e.target.value);
    if (type === 'focus') name && this.onInputFocus(name);
    if (type === 'blur') name && this.onInputBlur(name);
  }

  validate(name, value) {
    const valid = this.validation.validate(value, name);

    return valid;
  }

  onErrorMessageClose = name => {
    this.changeErrorVisibility(name, false);
  };

  changeErrorVisibility(name, bool) {
    const { fields } = this.state;
    const field = fields[name];

    this.setState({
      fields: {
        ...fields,
        [name]: {
          ...field,
          showErrors: bool
        }
      }
    });
  }

  isVisibleError(name) {
    const {
      app: { lang }
    } = this.props;
    const { fields, form } = this.state;
    const field = fields[name];

    return !field.isValid && (form.showErrors || field.showErrors);
  }

  onDisabledButtonClick = e => {
    const {
      actions,
      app: { lang }
    } = this.props;

    const fields = this.validation.validateForm(this.state.fields);

    this.setState({
      fields,
      form: { showErrors: true }
    });

    if (!this.isValid) {
      actions.showNotification(i18n('check_correct_fields', lang), 'error');
    }
  };

  onSubmit = e => {
    const {
      client,
      actions,
      app: { lang }
    } = this.props;
    const {
      fields: { email, password }
    } = this.state;

    e.preventDefault();

    if (this.isSent) return false;

    if (this.isValid) {
      this.isSent = true;
      client
        .mutate({
          mutation: GET_TOKEN,
          variables: {
            email: email.value.trim(),
            password: password.value.trim()
          }
        })
        .then(res => {
          if (res.data) {
            if (res.data.getToken) {
              signIn(res.data.getToken);
            } else {
              actions.showNotification(i18n('invalid_login_or_password', lang), 'error');
              this.isSent = false;
            }
          } else {
            actions.showNotification(i18n('request_failed', lang), 'error');
            this.isSent = false;
          }
        });
    } else {
      this.onDisabledButtonClick();
    }
  };

  render() {
    const {
      fields: { email, password }
    } = this.state;

    const {
      app: { lang }
    } = this.props;

    return (
      <Block.Root>
        <Block.NYImageWrapper>
          <img src={'/static/images/ny-main.jpg'} />
        </Block.NYImageWrapper>
        <Block.Wrapper>
          <Block.Headline>
            <Block.HeadLineQuestion />
            <Block.HeadLineText>{i18n('hello', lang)}</Block.HeadLineText>
          </Block.Headline>
          <Block.ContentWrapper noValidate as={'form'} onSubmit={this.onSubmit}>
            <Block.InputWrapper>
              <Block.Inputs>
                <Block.InputItem>
                  <TextInput
                    name={'email'}
                    value={email.value}
                    invalid={this.isVisibleError('email')}
                    errorMessage={this.isVisibleError('email') && email.errorMessage}
                    onAnyEvent={this.onAnyInputEvent.bind(this)}
                    placeholder={i18n('email', lang)}
                    type={'email'}
                  />
                </Block.InputItem>
                <Block.InputItem>
                  <TextInput
                    name={'password'}
                    value={password.value}
                    invalid={this.isVisibleError('password')}
                    errorMessage={this.isVisibleError('password') && password.errorMessage}
                    onAnyEvent={this.onAnyInputEvent.bind(this)}
                    placeholder={i18n('password', lang)}
                    type={'password'}
                  />
                </Block.InputItem>
              </Block.Inputs>
              <Block.ButtonItem>
                {!this.isValid && <Block.DisabledButton onClick={this.onDisabledButtonClick} />}
                <Button
                  fit={true}
                  type={'submit'}
                  view="common-new"
                  disabled={!this.isValid}
                  onClick={this.onSubmit}
                >
                  {i18n('login', lang)}
                </Button>
              </Block.ButtonItem>
            </Block.InputWrapper>
            <Link passHref route={'/reset-password'}>
              <Block.ForgotPassword as={'a'}>
                <Text view={'upperSpacing'}>{i18n('forget_password', lang)}</Text>
              </Block.ForgotPassword>
            </Link>
          </Block.ContentWrapper>
        </Block.Wrapper>
      </Block.Root>
    );
  }
}
