import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { formatTime } from 'lib/utils';
import Block from './StatusHistory.style';
import { connect } from 'react-redux';
import { i18n } from 'lib/constants';

const mapStateToProps = state => {
  return {
    app: state.app
  };
};

@connect(mapStateToProps)
export default class StatusHistory extends React.PureComponent {
  static propTypes = {
    data: PropTypes.any
  };

  static defaultProps = {
    data: []
  };

  render() {
    const {
      data,
      app: { lang }
    } = this.props;

    return (
      <Block.Root>
        <Block.Table>
          <tbody>
            {data.map(row => {
              const { date: _date, id, title, type, status, comment } = row;
              const date = _date ? moment(formatTime(_date)).format('DD.MM.YY') : undefined;

              return (
                <Block.Tr status={status} key={id + type}>
                  <Block.Td>
                    <Block.Title>{i18n(type, lang)}</Block.Title>
                  </Block.Td>
                  <Block.Td>{date && <Block.Date>{date}</Block.Date>}</Block.Td>
                  <Block.Td>
                    <Block.Status>{title}</Block.Status>
                  </Block.Td>
                </Block.Tr>
              );
            })}
          </tbody>
        </Block.Table>
      </Block.Root>
    );
  }
}
