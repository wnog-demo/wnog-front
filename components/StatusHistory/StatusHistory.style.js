import styled, { css } from 'styled-components';
import { STAGE_STATUSES } from 'lib/constants';

const StatusHistory = {};

StatusHistory.Table = styled.table`
  width: 100%;
`;

const colors = {
  [STAGE_STATUSES.FAILURE]: 'red',
  [STAGE_STATUSES.SUCCESS]: 'green',
  [STAGE_STATUSES.PROCESS]: 'yellow'
};

StatusHistory.Td = styled.td`
  padding: 0 20px 30px 0;
  position: relative;
`;

StatusHistory.Tr = styled.tr`
  ${({ status, theme: { color } }) => css`
    ${StatusHistory.Td} {
      :first-child {
        padding-left: 20px;
        :after {
          content: '';
          position: absolute;
          left: 0;
          top: 4px;
          bottom: 0;
          border-left: 2px dashed ${color[colors[status]]};
        }
        :before {
          content: '';
          position: absolute;
          height: 7px;
          width: 7px;
          left: -2.5px;
          top: 4px;
          border-radius: 50%;
          background-color: ${color[colors[status]]};
        }
      }
    }
  `}
  :last-child {
    ${StatusHistory.Td} {
      padding-bottom: 0;
      :after {
        display: none;
      }
    }
  }
`;

StatusHistory.Title = styled.div`
  font-size: 10px;
  font-weight: bold;
  letter-spacing: 0.05em;
  white-space: nowrap;
`;

StatusHistory.Date = styled.div`
  font-size: 9px;
  font-weight: 600;
  opacity: 0.5;
  white-space: nowrap;
`;

StatusHistory.Status = styled.div`
  font-size: 10px;
  font-weight: 600;
  opacity: 0.5;
`;

StatusHistory.Root = styled.div``;

export default StatusHistory;
