import styled, { css, keyframes } from 'styled-components';

const DetailHeader = {};

DetailHeader.Root = styled.div`
  position: relative;
  color: white;
`;

DetailHeader.CloseButton = styled.button`
  position: absolute;
  right: 40px;
  top: 40px;
  width: 44px;
  height: 44px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 12px;
  ${({ theme: { color } }) => css`
    background-color: ${color.white};
    transition: all 0.2s ease;
    color: ${color.darkBlue};
  `}
`;

DetailHeader.Wrapper = styled.div`
  width: 100%;
  max-width: 1100px;
  padding: 3.5rem 5.5rem 3.5rem 10.5rem;
  margin: 0 auto;
`;

DetailHeader.Flex = styled.div`
  display: flex;
`;

DetailHeader.Date = styled.div`
  display: flex;
  padding: 3px 10px;
  background-color: rgba(255, 255, 255, 0.2);
  border-radius: 39px;
  margin-right: 15px;
`;

DetailHeader.TagWrapper = styled.div`
  display: flex;
  margin-bottom: 1.125rem;
  align-items: center;
`;

DetailHeader.TagItem = styled.div`
  margin-right: 1rem;
  opacity: 0.4;

  :last-child {
    margin-right: 0;
  }
`;

DetailHeader.Headline = styled.div`
  text-align: center;
  margin-bottom: 1.5rem;
  //opacity: 0;

  ${({ isActive, clickedRowBounds }) => {
    const anim = keyframes`
    0%{
      
    }
    100%{
      
    }
  `;

    return css`
      ${isActive && css``}
    `;
  }}
`;

DetailHeader.InfoWrapper = styled.div`
  @media (max-width: 1270px) {
    flex-direction: column;
    align-items: flex-start;
  }
`;

DetailHeader.InfoItem = styled.div`
  display: flex;
  align-items: center;
  margin-right: 18px;
  padding-bottom: 10px;
  flex-wrap: wrap;
  :last-child {
    margin-right: 0;
  }
  @media (max-width: 1270px) {
    margin-bottom: 10px;
  }
`;

DetailHeader.From = styled.div``;

DetailHeader.AddressIcon = styled.div`
  margin: 0 0.5rem;
  line-height: 1;
`;

DetailHeader.To = styled.div``;

DetailHeader.SizeIcon = styled.div`
  margin-right: 0.75rem;
  line-height: 1;
`;

DetailHeader.Size = styled.div``;

DetailHeader.Separator = styled.div`
  width: 3px;
  height: 3px;
  border-radius: 50%;
  margin: 0 0.5rem;
`;

DetailHeader.Weight = styled.div``;

DetailHeader.ManagerIcon = styled.div`
  font-size: 14px;
  line-height: 1;
  margin-right: 0.8rem;
`;

DetailHeader.ManagerName = styled.div``;

DetailHeader.ManagerCompany = styled.div``;

export default DetailHeader;
