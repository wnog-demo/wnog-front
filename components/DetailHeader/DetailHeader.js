import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { Text, Icon } from 'elements';
import { Link } from 'lib/routes';
import { formatTime, declOfNum } from 'lib/utils';
import { i18n } from 'lib/constants';
import Block from './DetailHeader.style';
import { connect } from 'react-redux';

const mapStateToProps = state => {
  return {
    app: state.app
  };
};

@connect(mapStateToProps)
export default class DetailHeader extends React.PureComponent {
  static propTypes = {
    creationDate: PropTypes.number,
    reference: PropTypes.string,
    product: PropTypes.string,
    from: PropTypes.string,
    to: PropTypes.string,
    shipments: PropTypes.object,
    contact: PropTypes.object,
    isActive: PropTypes.bool
  };
  static defaultProps = {
    from: '?',
    to: '?'
  };
  render() {
    const {
      reference,
      creationDate: _creationDate,
      product,
      from,
      to,
      contact,
      shipments,
      coordinator,
      app: { lang },
      onCloseClick,
      isActive,
      clickedRowBounds
    } = this.props;

    const creationDate = moment(formatTime(_creationDate)).format('DD.MM.YY');

    return (
      <Block.Root>
        <Link route={'/'} passHref>
          <Block.CloseButton as={'a'} onClick={onCloseClick}>
            <Icon name="cross" />
          </Block.CloseButton>
        </Link>
        <Block.Wrapper>
          <Block.TagWrapper>
            <Block.Date>
              <Text view={'upperSpacing'} as={'h1'}>
                {i18n('created', lang)} {creationDate}
              </Text>
            </Block.Date>
            {product && (
              <Block.TagItem>
                <Text view={'upperSpacing'}>{product}</Text>
              </Block.TagItem>
            )}
          </Block.TagWrapper>
          <Block.Headline isActive={isActive} clickedRowBounds={clickedRowBounds}>
            <Text view={'h1Left'}>{reference}</Text>
          </Block.Headline>
          <Block.InfoWrapper>
            <Block.InfoItem>
              <Block.From>
                <Text>{from}</Text>
              </Block.From>
              {to && (
                <React.Fragment>
                  <Block.AddressIcon>
                    <Icon name="arrow" />
                  </Block.AddressIcon>
                  <Block.To>
                    <Text>{to}</Text>
                  </Block.To>
                </React.Fragment>
              )}
            </Block.InfoItem>
            <Block.Flex>
              {shipments && (
                <Block.InfoItem>
                  <Block.SizeIcon>
                    <Icon name="cube" />
                  </Block.SizeIcon>
                  <Block.Size>
                    <Text view={'common14'}>{shipments.quantity}</Text>
                  </Block.Size>
                  <Block.Separator />
                  <Block.Weight>
                    <Text view={'common14'}>{shipments.weight}</Text>
                  </Block.Weight>
                </Block.InfoItem>
              )}
              {contact && (
                <Block.InfoItem>
                  <Block.ManagerIcon>
                    <Icon name="person" />
                  </Block.ManagerIcon>
                  <Block.ManagerName>
                    <Text view={'common14'}>{contact.name}</Text>
                  </Block.ManagerName>
                  <Block.Separator />
                  <Block.ManagerCompany>
                    <Text view={'common14'}>WNOG</Text>
                  </Block.ManagerCompany>
                </Block.InfoItem>
              )}
            </Block.Flex>
          </Block.InfoWrapper>
        </Block.Wrapper>
      </Block.Root>
    );
  }
}
