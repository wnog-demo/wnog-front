import styled, { css } from 'styled-components';

const StatusSidebar = {};

StatusSidebar.Root = styled.div`
  height: 100%;
  ${({ theme: { color } }) => css`
    background-color: #f9fafb;
  `}
`;

StatusSidebar.Wrapper = styled.div`
  padding: 40px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding-bottom: 80px;
`;

StatusSidebar.Search = styled.div`
  margin-bottom: 2.25rem;
  width: 100%;
`;

StatusSidebar.Reset = styled.button`
  margin-bottom: 2rem;
  display: flex;
  align-items: center;
  transition: all 0.2s ease;
  ${({ theme: { color } }) => css`
    > span {
      color: inherit;
    }
    :hover {
      color: ${color.blue};
    }
  `}
`;

StatusSidebar.ResetIcon = styled.div`
  font-size: 22px;
  line-height: 1;
  margin-right: 10px;
  transition: all 0.2s ease;
  ${({ theme: { color } }) => css`
    color: ${color.black};
    ${StatusSidebar.Reset}:hover && {
      color: ${color.blue};
    }
  `}
`;

StatusSidebar.Point = styled.div`
  margin-left: -15px;
  padding-bottom: 1.5rem;
  position: relative;
  left: 15px;
  width: 100%;
`;

StatusSidebar.Name = styled.div`
  margin-bottom: 0.75rem;
`;

StatusSidebar.FilterWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin: 0 -4px;
  position: relative;
`;

StatusSidebar.ArchiveText = styled.div`
  font-size: 11px;
  text-transform: uppercase;
  letter-spacing: 0.1em;
  font-weight: 500;
  padding-left: 10px;
  transition: all 0.2s ease;
`;

StatusSidebar.DatePicker = styled.div`
  padding: 16px;
  border-top: 1px solid ${({ theme: { color } }) => color.sidebarBgGray};
`;

StatusSidebar.Checkbox = styled.div`
  margin: 16px;
`;

StatusSidebar.CheckboxText = styled.div`
  font-size: 11px;
  text-transform: uppercase;
  letter-spacing: 0.1em;
  font-weight: 500;
  padding-left: 10px;
  transition: all 0.2s ease;
  ${({ active, theme: { color } }) =>
    active
      ? css`
          color: ${color.blue};
        `
      : css`
          ${StatusSidebar.Checkbox}:hover & {
            color: ${color.blue};
          }
        `}
`;

StatusSidebar.ArchiveCheckbox = styled.div`
  display: inline-flex;
  ${({ isActive, theme: { color } }) => css`
    :hover {
      ${StatusSidebar.ArchiveText} {
        color: ${color.blue};
      }
    }
    ${isActive &&
      css`
        color: ${color.blue};
      `}
  `}
`;

StatusSidebar.FilterButton = styled.button`
  padding: 8px 16px;
  display: flex;
  text-transform: uppercase;
  font-weight: 500;
  font-size: 9px;
  line-height: 12px;
  letter-spacing: 0.14em;
  color: inherit;
  font-family: HelveticaNeue;
`;

StatusSidebar.Filter = styled.div`
  display: flex;
  align-items: center;
  margin: 0 4px 10px;
  border-radius: 39px;
  transition: all 0.2s ease;
  position: relative;
  z-index: 25;
  cursor: pointer;
  border: 1px solid rgba(44, 171, 252, 0.3);
  color: #576473;

  ${({ modal, active, theme: { color } }) =>
    modal &&
    css`
      &:before {
        content: '';
        position: absolute;
        top: 100%;
        border: 4px solid transparent;
        left: calc(50% - 4px);
        z-index: -1;
        transition: all 0.2s ease;
        ${active
          ? css`
              border-top: 4px solid ${color.blue};
            `
          : css`
              border-top: 4px solid ${color.white};
              transform: translateY(-100%);
            `}
      }
    `}

  :hover,
  :focus {
    > span {
      opacity: 1;
    }
  }

  ${({ active, modal, value, theme: { color } }) => css`
    ${modal &&
      value &&
      css`
        padding-right: 18px;
      `}

    ${active &&
      css`
        color: #2cabfc;
        border-color: #2cabfc;
        ${StatusSidebar.FilterButton} {
          > span {
            color: white;
            opacity: 1;
          }
        }
      `}
  `}
`;

StatusSidebar.Popup = styled.div`
  position: absolute;
  background-color: #fff;
  left: 4px;
  right: 4px;
  z-index: 50;
  top: calc(100% + 6px);
  border-radius: 5px;
  transition: all 0.2s ease;
  ${({ state: { active } }) =>
    !active &&
    css`
      pointer-events: none;
      opacity: 0;
      transform: translateY(10px);
    `}
`;

StatusSidebar.Cross = styled.div`
  width: 30px;
  height: 30px;
  top: 0;
  right: 0;
  font-size: 8px;
  padding-bottom: 1px;
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all 0.2s ease;
  color: #2cabfc;
  cursor: pointer;
  :hover {
    color: red;
  }
  ${({ active }) =>
    !active &&
    css`
      opacity: 0;
      pointer-events: none;
      transform: translateX(5px);
    `}
`;

export default StatusSidebar;
