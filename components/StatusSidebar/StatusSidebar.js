import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { debounce } from 'throttle-debounce';
import moment from 'moment';

import Block from './StatusSidebar.style';
import actionsCreators from 'store/actions';
import { Text, Icon, TextInput, Checkbox, Scrollbar, DatePicker } from 'elements';
import { i18n } from 'lib/constants';

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      changeFilterOption: bindActionCreators(actionsCreators.changeFilterOption, dispatch),
      resetFilter: bindActionCreators(actionsCreators.resetFilter, dispatch),
      changeSearchQuery: bindActionCreators(actionsCreators.changeSearchQuery, dispatch)
    }
  };
};

const mapStateToProps = state => {
  return {
    app: state.app,
    filter: state.filter
  };
};

@connect(
  mapStateToProps,
  mapDispatchToProps
)
export default class StatusSidebar extends React.PureComponent {
  calendarRef = React.createRef();

  static propTypes = {
    data: PropTypes.any
  };

  constructor(props) {
    super(props);

    this.inputRef = React.createRef();

    this.state = {
      active: [],
      isArchiveEnabled: false,
      query: props.filter.query,
      popup: null
    };

    this.event = {
      closePopup: ({ target }) => {
        if (target && !target.closest('[data-popup-ignore]')) {
          this.closePopup();
        }
      }
    };
  }

  componentDidUpdate(prevProps, prevState) {
    // если вдруг поле поиска меняется не через инпут (например, когда сбрасываем фильтры в другом компоненте)
    const stateIsChanged = prevState.query !== this.state.query;
    const propsIsChanged = prevProps.filter.query !== this.props.filter.query;
    const isEqual = this.state.query === this.props.filter.query;

    if (propsIsChanged && !stateIsChanged && !isEqual) {
      this.setState({
        query: this.props.filter.query
      });
    }
  }

  onFilterOptionClick = (name, value) => {
    const {
      actions: { changeFilterOption },
      filter
    } = this.props;

    const option = [...(filter.options[name] || [])];
    const isIncludes = option.includes(value);
    const valueIndex = option.findIndex(item => item === value);

    if (isIncludes) option.splice(valueIndex, 1);
    const newOptionState = isIncludes ? option : [...option, value];
    changeFilterOption(name, newOptionState);
    document.scrollingElement.scrollTop = 0;
  };

  onFilterDateClick = (name, value, subname) => {
    const {
      actions: { changeFilterOption }
    } = this.props;

    if (typeof value === 'object' && !value.to && !value.from) {
      value = false;
    }

    changeFilterOption(name, value, subname);
  };

  onResetButtonClick() {
    const { actions } = this.props;

    actions.resetFilter();

    document.scrollingElement.scrollTop = 0;
  }

  onArchiveCheckboxChange = e => {
    const {
      actions: { changeFilterOption },
      filter
    } = this.props;

    this.setState({
      isArchiveEnabled: !this.state.isArchiveEnabled
    });

    changeFilterOption('is_archive', !this.state.isArchiveEnabled);
  };

  onSearchQueryChange = value => {
    this.setState({
      query: value
    });
    this.changeSearchQuery(value);
  };

  changeSearchQuery = debounce(300, value => {
    const {
      actions: { changeSearchQuery }
    } = this.props;

    changeSearchQuery(value);
  });

  onTogglePopup(name) {
    this.setState(
      {
        popup: this.state.popup !== name ? name : null
      },
      () => {
        if (this.state.popup) {
          document.body.addEventListener('click', this.event.closePopup);
        }
      }
    );
  }

  onCancelClick = (e, value) => {
    this.cancelBubble(e);
    this.closePopup();
    this.onFilterDateClick(value, false);
  };

  cancelBubble = e => {
    // ie11
    e.returnValue = false;
    e.cancelBubble = true;
    e.stopPropagation();
    e.preventDefault();
  };

  onPopUpClick = e => {
    // this.cancelBubble(e);
  };

  closePopup() {
    document.body.removeEventListener('click', this.event.closePopup);

    this.setState({
      popup: null
    });
  }

  renderMain() {
    const {
      data: { filter: data },
      filter: { options: filterOptions, query },
      app: { lang }
    } = this.props;

    const list = JSON.parse(data);

    return (
      <React.Fragment>
        <Block.Search>
          <TextInput
            placeholder={i18n('client_ref', lang)}
            view={'search'}
            icon={'search'}
            onChange={this.onSearchQueryChange}
            value={this.state.query}
            ref={this.inputRef}
          />
        </Block.Search>
        {['types', 'greenlights', 'comings', 'declarations', 'customs', 'transportations'].map(
          key => {
            const title = key;
            const items = list[key].items || list[key];

            if (!items) return;

            return (
              <Block.Point key={title}>
                <Block.Name>
                  <Text view={'boldLh16'}>{i18n(title, lang)}</Text>
                </Block.Name>
                <Block.FilterWrapper>
                  {items.map(item => {
                    const { title: name, value } = item;

                    if (title !== 'comings') {
                      const isActive = filterOptions[key] && filterOptions[key].includes(value);

                      return (
                        <Block.Filter
                          active={isActive}
                          onClick={e => this.onFilterOptionClick(key, value)}
                          key={name}
                        >
                          <Block.FilterButton>{name}</Block.FilterButton>
                        </Block.Filter>
                      );
                    } else {
                      const isActiveBool =
                        filterOptions[value] && typeof filterOptions[value] === 'boolean';

                      const isActiveObj =
                        filterOptions[value] && typeof filterOptions[value] === 'object';

                      const valueObj = isActiveObj ? filterOptions[value] : {};

                      const isActiveAll = isActiveBool || !!isActiveObj;

                      return (
                        <React.Fragment key={name}>
                          <Block.Filter
                            active={name === this.state.popup || isActiveAll}
                            value={isActiveAll}
                            modal={true}
                            key={name}
                            data-popup-ignore={true}
                            ref={this.calendarRef}
                          >
                            <React.Fragment>
                              <Block.FilterButton
                                onClick={e => {
                                  if (!e.target.closest('[data-cross]')) {
                                    this.onTogglePopup(name);
                                  }
                                }}
                              >
                                <>
                                  {name}
                                  {valueObj && (
                                    <>
                                      {' '}
                                      {valueObj.from && moment(valueObj.from).format('DD.MM')}
                                      {valueObj.to && '—' + moment(valueObj.to).format('DD.MM')}
                                    </>
                                  )}
                                </>
                              </Block.FilterButton>
                              <Block.Cross
                                active={isActiveAll}
                                onClick={e => this.onFilterDateClick(value, false)}
                                data-cross={true}
                              >
                                <span>
                                  <Icon name={'cross'} />
                                </span>
                              </Block.Cross>
                            </React.Fragment>
                          </Block.Filter>
                          <Block.Popup
                            state={{ active: name === this.state.popup }}
                            data-popup-ignore={true}
                            onClick={this.onPopUpClick}
                          >
                            <Block.Checkbox>
                              <Checkbox
                                value={isActiveBool}
                                view={'blue'}
                                onChange={e => this.onFilterDateClick(value, !isActiveBool)}
                              >
                                <Block.CheckboxText active={isActiveBool}>
                                  {i18n('all_time', lang)}
                                </Block.CheckboxText>
                              </Checkbox>
                            </Block.Checkbox>
                            <Block.DatePicker>
                              <DatePicker
                                {...valueObj}
                                onChange={day => this.onFilterDateClick(value, day)}
                                locale={lang}
                              />
                            </Block.DatePicker>
                          </Block.Popup>
                        </React.Fragment>
                      );
                    }
                  })}
                </Block.FilterWrapper>
              </Block.Point>
            );
          }
        )}
        <Block.Reset onClick={e => this.onResetButtonClick()}>
          <Block.ResetIcon>
            <Icon name="reset" />
          </Block.ResetIcon>
          <Text view={'lang'} as={'span'}>
            {i18n('reset_filter', lang)}
          </Text>
        </Block.Reset>
        <Block.ArchiveCheckbox isActive={filterOptions.is_archive}>
          <Checkbox
            onChange={this.onArchiveCheckboxChange}
            value={filterOptions.is_archive}
            view={'blue'}
          >
            <Block.ArchiveText>{i18n('archive', lang)}</Block.ArchiveText>
          </Checkbox>
        </Block.ArchiveCheckbox>
      </React.Fragment>
    );
  }

  render() {
    const {
      data,
      app: { lang }
    } = this.props;

    return (
      <Block.Root>
        <Scrollbar>
          <Block.Wrapper>
            {data.loading && <Text>{i18n('loading', lang)}...</Text>}
            {!data.loading && data.filter && this.renderMain()}
          </Block.Wrapper>
        </Scrollbar>
      </Block.Root>
    );
  }
}
