import styled, { css } from 'styled-components';

const PasswordReset = {};

PasswordReset.Root = styled.div`
  ${({ theme: { color } }) => css`
    background-color: ${color.bgGray};
  `}
`;

PasswordReset.Wrapper = styled.div`
  padding-top: 6rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

PasswordReset.Headline = styled.div`
  text-align: center;
  max-width: 59%;
  margin-bottom: 1.5rem;
`;

PasswordReset.Text = styled.div`
  margin-bottom: 3rem;
  max-width: 440px;
`;

PasswordReset.ContentWrapper = styled.div`
  margin: 0 -10px;
`;

PasswordReset.InputWrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  margin: 0 -10px 28px;
`;

PasswordReset.InputItem = styled.div`
  display: flex;
  margin: 0 10px;
  width: 260px;
  flex-shrink: 0;
`;

PasswordReset.ButtonItem = styled.div`
  display: flex;
  margin: 0 10px;
  flex-shrink: 0;
  position: relative;
`;

PasswordReset.DisabledButton = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
`;

PasswordReset.EmailWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  margin-bottom: 2.87rem;
`;

PasswordReset.Email = styled.div`
  display: flex;
  align-items: center;
`;

PasswordReset.EmailNote = styled.div`
  margin-left: 4px;
`;

PasswordReset.EmailIcon = styled.div`
  font-size: 1em;
  line-height: 1;
  margin-right: 0.75rem;
  ${({ theme: { color } }) => css`
    color: ${color.blue};
  `}
`;

export default PasswordReset;
