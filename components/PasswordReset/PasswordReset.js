import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql, compose, withApollo } from 'react-apollo';

import actionsCreators from 'store/actions';
import { Text, Icon, TextInput, Button } from 'elements';
import { Validation } from 'lib/utils';
import { RESTORE_PASSWORD } from 'graphql/mutation';
import Block from './PasswordReset.style';
import { i18n } from 'lib/constants';

const field = {
  value: '',
  isValid: false,
  showErrors: false,
  errorMessage: ''
};

const validationOptions = {
  email: {
    options: {
      required: true,
      email: true
    }
  }
};

const mapStateToProps = state => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      showNotification: bindActionCreators(actionsCreators.showNotification, dispatch)
    }
  };
};

@connect(
  mapStateToProps,
  mapDispatchToProps
)
@withApollo
export default class PasswordReset extends React.PureComponent {
  isSent = false;

  state = {
    form: { showErrors: false },
    mailSent: false,
    fields: {
      email: { ...field }
    }
  };

  get isValid() {
    const { fields } = this.state;

    return !Object.keys(validationOptions).some(name => {
      return !fields[name].isValid;
    });
  }

  componentDidMount() {
    const {
      app: { lang }
    } = this.props;
    this.validation = new Validation({ config: validationOptions, lang });
  }

  onInputChange(name, value) {
    const {
      app: { lang }
    } = this.props;
    const fields = this.validation.onInputChange(this.state.fields, name, value);

    this.setState({ fields });
  }

  onInputFocus(name) {
    this.changeErrorVisibility(name, false);
  }

  onInputBlur(name) {
    this.changeErrorVisibility(name, true);
  }

  onAnyInputEvent(type, name, e) {
    if (type === 'change') name && this.onInputChange(name, e.target.value);
    if (type === 'focus') name && this.onInputFocus(name);
    if (type === 'blur') name && this.onInputBlur(name);
  }

  validate(name, value) {
    const {
      app: { lang }
    } = this.props;
    const valid = this.validation.validate(value, name);

    return valid;
  }

  onErrorMessageClose = name => {
    this.changeErrorVisibility(name, false);
  };

  changeErrorVisibility(name, bool) {
    const { fields } = this.state;
    const field = fields[name];

    this.setState({
      fields: {
        ...fields,
        [name]: {
          ...field,
          showErrors: bool
        }
      }
    });
  }

  isVisibleError(name) {
    const { fields, form } = this.state;
    const field = fields[name];

    return !field.isValid && (form.showErrors || field.showErrors);
  }

  onDisabledButtonClick = e => {
    const {
      actions,
      app: { lang }
    } = this.props;

    const fields = this.validation.validateForm(this.state.fields);

    this.setState({
      fields,
      form: { showErrors: true }
    });

    if (!this.isValid) {
      actions.showNotification(i18n('check_correct_fields', lang), 'error');
    }
  };

  onSubmit = e => {
    const {
      fields: { email }
    } = this.state;
    const {
      client,
      actions,
      app: { lang }
    } = this.props;

    e.preventDefault();

    if (this.isSent) return false;

    if (!this.isValid) {
      this.onDisabledButtonClick();
      return false;
    }

    this.isSent = true;

    client
      .mutate({
        mutation: RESTORE_PASSWORD,
        variables: {
          email: email.value.trim()
        }
      })
      .then(res => {
        if (res.data && res.data.restorePassword) {
          actions.showNotification(i18n('submitted', lang));
          this.setState({
            mailSent: true
          });
        } else {
          actions.showNotification(i18n('user_email_without', lang), 'error');
          this.isSent = false;
        }
      });
  };

  render() {
    const {
      fields: { email },
      mailSent
    } = this.state;

    const {
      app: { lang }
    } = this.props;

    return (
      <Block.Root>
        <Block.Wrapper>
          <Block.Headline>
            <Text view={'h1'}>
              {!mailSent ? i18n('restore_access', lang) : i18n('email_sent', lang)}
            </Text>
          </Block.Headline>
          {!mailSent && (
            <Block.Text>
              <Text view={'common'}>{i18n('email_sent_link', lang)}</Text>
            </Block.Text>
          )}
          {mailSent && (
            <Block.EmailWrapper>
              <Block.Email>
                <Block.EmailIcon>
                  <Icon name="mail" />
                </Block.EmailIcon>
                <Text view="upperSpacing">{email.value}</Text>
              </Block.Email>
              <Block.EmailNote>
                <Text view="note">— {i18n('to_email', lang)}</Text>
              </Block.EmailNote>
            </Block.EmailWrapper>
          )}
          {!mailSent && (
            <Block.InputWrapper noValidate as={'form'} onSubmit={this.onSubmit}>
              <Block.InputItem>
                <TextInput
                  name={'email'}
                  value={email.value}
                  invalid={this.isVisibleError('email')}
                  errorMessage={this.isVisibleError('email') && email.errorMessage}
                  onAnyEvent={this.onAnyInputEvent.bind(this)}
                  placeholder={'Email'}
                  type={'email'}
                  icon="mail"
                />
              </Block.InputItem>
              <Block.ButtonItem>
                {!this.isValid && <Block.DisabledButton onClick={this.onDisabledButtonClick} />}
                <Button
                  type={'submit'}
                  disabled={!this.isValid}
                  onClick={this.onSubmit}
                  view="common"
                >
                  {i18n('send', lang)}
                </Button>
              </Block.ButtonItem>
            </Block.InputWrapper>
          )}
        </Block.Wrapper>
      </Block.Root>
    );
  }
}
