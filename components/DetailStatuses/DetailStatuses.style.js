import styled, { css } from 'styled-components';
import { STAGE_STATUSES } from 'lib/constants';

const DetailSort = {};

DetailSort.Root = styled.div`
  display: flex;
  flex-grow: 1;
  ${({ theme: { color } }) => css`
    background-color: ${color.white};
    border-bottom: 2px solid ${color.bgGray};
  `}
`;

DetailSort.Wrapper = styled.div`
  width: 100%;
  max-width: 1100px;
  padding: 3rem 3rem 3.5rem 10.5rem;
  margin: 0 auto;
`;

DetailSort.Note = styled.div`
  margin-top: 0.5rem;
`;

DetailSort.Header = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 2rem;
`;

DetailSort.ReverseIcon = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

DetailSort.RevertButton = styled.button`
  width: 28px;
  height: 28px;
  border-radius: 50%;
  font-size: 9px;
  position: relative;
  overflow: hidden;

  ${({ isReversed, theme: { color } }) => css`
    background-color: ${color.sidebarBgGray};
    color: ${color.black};
    :hover {
      background-color: ${color.blue};
      color: white;
    }
    ${isReversed
      ? css`
          ${DetailSort.ReverseIcon} {
            transform: rotate(-180deg);
          }
        `
      : css`
          ${DetailSort.ReverseIcon} {
            transform: none;
          }
        `}
  `}
`;

DetailSort.Headline = styled.h2`
  text-align: left;
  margin-left: 55px;
`;

DetailSort.Sort = styled.div`
  margin-left: 12px;
`;

const colors = {
  [STAGE_STATUSES.FAILURE]: 'red',
  [STAGE_STATUSES.PROCESS]: 'yellow',
  [STAGE_STATUSES.SUCCESS]: 'green'
};

DetailSort.SortPoint = styled.div`
  position: absolute;
  top: 0;
  left: -5px;
  width: 8px;
  height: 8px;
  border-radius: 50%;
  ${({ theme: { color } }) => css`
    background-color: ${color.lightgray};
  `}
`;

DetailSort.SortItem = styled.div`
  display: flex;
  width: 100%;
  position: relative;
  padding-left: 70px;
  padding-bottom: 2rem;
  border-left: 2px dashed;
  ${({ status, theme: { color } }) => css`
    ${status &&
      css`
        border-color: ${color[colors[status]]};
        ${DetailSort.SortPoint} {
          background-color: ${color[colors[status]]};
        }
      `}
  `}
  :last-child {
    border-color: transparent;
  }
`;

DetailSort.Info = styled.div`
  width: 30%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

DetailSort.Name = styled.div`
  margin-bottom: 0.75rem;
`;

DetailSort.Hash = styled.div``;

DetailSort.Events = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
`;

DetailSort.EventItem = styled.div`
  display: flex;
  align-items: flex-start;
  margin-bottom: 1rem;
`;

DetailSort.DateWrapper = styled.div`
  width: 20%;
  display: flex;
`;

DetailSort.Date = styled.div`
  flex-shrink: 0;
  margin-right: 1.5rem;
  margin-left: auto;
  border-radius: 16px;
  padding: 2px 6px;
  ${({ theme: { color } }) => css`
    background-color: ${color.bgGray};
  `}
`;

DetailSort.State = styled.div`
  text-align: left;
  width: 65%;
  font-size: 0;

  > span {
    line-height: 16px;
  }
`;

export default DetailSort;
