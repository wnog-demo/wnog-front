import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { STAGE_NAMES, STAGE_STATUSES, STAGES } from 'lib/constants';
import Block from './DetailStatuses.style';
import { Text, Icon, TextInput, Button } from 'elements';
import { getLast, formatTime } from 'lib/utils';
import { i18n } from 'lib/constants';
import { connect } from 'react-redux';

const mapStateToProps = state => {
  return {
    app: state.app
  };
};

@connect(mapStateToProps)
export default class DetailStatuses extends React.PureComponent {
  state = {
    isReversedSorting: false
  };

  static propTypes = {
    stages: PropTypes.any,
    lastStages: PropTypes.any,
    dtNumber: PropTypes.string
  };

  onReverseToggle = e => {
    this.setState({
      isReversedSorting: !this.state.isReversedSorting
    });
  };

  sort = (a, b) => {
    const { isReversedSorting } = this.state;

    return isReversedSorting ? b.date - a.date : a.date - b.date;
  };

  render() {
    const { isReversedSorting } = this.state;
    const {
      stages,
      app: { lang }
    } = this.props;

    return (
      <Block.Root>
        <Block.Wrapper>
          <Block.Header>
            <Block.RevertButton
              isReversed={this.state.isReversedSorting}
              onClick={this.onReverseToggle}
            >
              <Block.ReverseIcon>
                <Icon name="smallArrow" />
              </Block.ReverseIcon>
            </Block.RevertButton>
            <Block.Headline>
              <Text view={'detailHeadline'}>{i18n('stage_title', lang)}</Text>
            </Block.Headline>
          </Block.Header>
          <Block.Sort>
            {(isReversedSorting ? [...stages].reverse() : stages).map(item => {
              if (!item.list.length === 0) return;

              const last = item.list[item.list.length - 1];

              return (
                <Block.SortItem status={last && last.status} key={item.type + Math.random()}>
                  <Block.SortPoint />
                  <Block.Info>
                    <Block.Name>
                      <Text view={'bold'}>{i18n(item.type, lang)}</Text>
                    </Block.Name>
                    {item.number && (
                      <Block.Hash>
                        <Text view={'opacity'}>{item.number}</Text>
                      </Block.Hash>
                    )}
                  </Block.Info>
                  <Block.Events>
                    {(isReversedSorting ? [...item.list].reverse() : item.list).map(status => {
                      const { title, date: _date, id, uid, comment } = status;
                      const date = _date ? moment(formatTime(_date)).format('DD.MM.YY') : undefined;

                      return (
                        <Block.EventItem key={uid + Math.random()}>
                          <Block.DateWrapper>
                            {date && (
                              <Block.Date>
                                <Text view={'date'}>{date}</Text>
                              </Block.Date>
                            )}
                          </Block.DateWrapper>
                          <Block.State>
                            <Text view={'bold'}>{title}</Text>
                            {comment && (
                              <Block.Note>
                                <Text view={'note'}>{comment}</Text>
                              </Block.Note>
                            )}
                          </Block.State>
                        </Block.EventItem>
                      );
                    })}
                  </Block.Events>
                </Block.SortItem>
              );
            })}
          </Block.Sort>
        </Block.Wrapper>
      </Block.Root>
    );
  }
}
