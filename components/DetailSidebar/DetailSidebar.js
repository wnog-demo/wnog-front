import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Block from './DetailSidebar.style';
import { Text, Icon, TextInput, Button } from 'elements';
import { i18n, TRANSPORT_TYPES } from 'lib/constants';
import { connect } from 'react-redux';

const routeIcons = {
  start: 'ellipse',
  progress: 'vector',
  finish: 'flag'
};

const routeTypeIcons = {
  [TRANSPORT_TYPES.PLANE]: 'plane',
  [TRANSPORT_TYPES.CAR]: 'car',
  [TRANSPORT_TYPES.SHIP]: 'ship',
  [TRANSPORT_TYPES.TRAIN]: 'train'
};

const mapStateToProps = state => {
  return {
    app: state.app
  };
};

@connect(mapStateToProps)
export default class DetailSidebar extends React.PureComponent {
  static propTypes = {
    custom: PropTypes.object,
    route: PropTypes.object
  };
  render() {
    const {
      custom,
      route,
      vessel,
      type,
      app: { lang }
    } = this.props;

    const { type: routeType, list, number } = route || {};
    const routeTypeIcon = routeTypeIcons[routeType];
    const isShip = TRANSPORT_TYPES.SHIP === routeType;

    return (
      <Block.Root>
        <Block.Wrapper>
          <Block.Title>{i18n(type, lang)}</Block.Title>
          <Block.TransportType>
            {custom && (
              <Block.Point noBorder={true}>
                <Block.PointIcon>
                  <Block.Label>{custom.name}</Block.Label>
                </Block.PointIcon>
                <Block.PointInfo>
                  <Block.Name>
                    <Text view={'boldLh16'}>{lang === 'en' ? custom.city : custom.fullname}</Text>
                  </Block.Name>
                  <Block.Note>
                    <Text view={'note'}>{i18n('customs', lang)}</Text>
                  </Block.Note>
                </Block.PointInfo>
              </Block.Point>
            )}

            <Block.Point noBorder={true}>
              <Block.TransportTypeIcon>
                <Icon name={routeTypeIcon} />
              </Block.TransportTypeIcon>
              <Block.PointInfo>
                <Block.Name>
                  <Text view={'boldLh16'} uppercase={true}>
                    {isShip && vessel ? vessel.name : routeType}
                  </Text>
                </Block.Name>
                <Block.Note>
                  <Text view={'note'}>
                    {isShip && vessel && routeType.toUpperCase() + ': '}
                    {number}
                  </Text>
                </Block.Note>
              </Block.PointInfo>
            </Block.Point>
          </Block.TransportType>
          {list && (
            <React.Fragment>
              <Block.Title>{i18n('route', lang)}</Block.Title>
              <Block.PointWrapper>
                {list.map((point, i) => {
                  let icon = '';
                  if (i === 0) icon = 'ellipse';
                  if (i > 0) icon = 'vector';
                  if (i !== 0 && i === list.length - 1) icon = 'flag';
                  const { name, title } = point;

                  return (
                    <Block.Point>
                      <Block.PointIcon>
                        <Icon name={icon} />
                      </Block.PointIcon>
                      <Block.PointInfo>
                        {name && (
                          <Block.Name>
                            <Text view={'boldLh16'}>{name}</Text>
                          </Block.Name>
                        )}
                        {title && (
                          <Block.Note>
                            <Text view={'note'}>{title}</Text>
                          </Block.Note>
                        )}
                      </Block.PointInfo>
                    </Block.Point>
                  );
                })}
              </Block.PointWrapper>
            </React.Fragment>
          )}
        </Block.Wrapper>
      </Block.Root>
    );
  }
}
