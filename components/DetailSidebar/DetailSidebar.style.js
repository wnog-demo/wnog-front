import styled, { css } from 'styled-components';

const DetailSidebar = {};

DetailSidebar.Root = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  ${({ theme: { color } }) => css`
    background-color: ${color.sidebarBgGray};
  `}
`;

DetailSidebar.Wrapper = styled.div`
  padding: 50px;
`;

DetailSidebar.Title = styled.div`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 2.5rem;
`;

DetailSidebar.PointWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

DetailSidebar.Point = styled.div`
  display: flex;
  width: 100%;
  position: relative;
  padding-left: 50px;
  padding-bottom: 2.25rem;
  ${({ noBorder, theme: { color } }) => css`
    ${!noBorder &&
      css`
        border-left: 2px dashed ${color.blue};
      `}
    :last-child {
      border-color: transparent;
    }
  `}
`;

DetailSidebar.PointIcon = styled.div`
  font-size: 14px;
  line-height: 1;
  position: absolute;
  left: -8px;
  top: -2px;
  z-index: 3;
  padding: 3px 0;
  ${({ theme: { color } }) => css`
    color: ${color.blue};
    background-color: ${color.sidebarBgGray};
  `}
`;

DetailSidebar.PointInfo = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  z-index: 50;
  width: 100%;
`;

DetailSidebar.Name = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 4px;
  line-height: 16px;
`;

DetailSidebar.Label = styled.div`
  padding: 4px 6px;
  border-radius: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 8px;
  font-size: 9px;
  text-transform: uppercase;
  font-weight: bold;
  letter-spacing: 1px;
  ${({ theme: { color } }) => css`
    background-color: ${color.darkgray};
    color: ${color.white};
  `}
`;

DetailSidebar.Note = styled.div`
  line-height: 1.35;
`;

DetailSidebar.TransportType = styled.div`
  margin-top: 4px;
`;

DetailSidebar.TransportTypeIcon = styled(DetailSidebar.PointIcon)`
  font-size: 24px;
  left: 0;
`;

export default DetailSidebar;
