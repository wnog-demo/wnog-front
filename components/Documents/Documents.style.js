import styled, { css } from 'styled-components';

const Documents = styled.div``;

Documents.Wrapper = styled.div`
  width: 100%;
  max-width: 1100px;
  padding: 3rem 3rem 3.5rem 10.5rem;
  margin: 0 auto;
`;

Documents.Head = styled.div`
  display: flex;
  align-items: center;
  padding-bottom: 2rem;
`;

Documents.Title = styled.div`
  width: 300px;
  padding-right: 20px;
  flex-shrink: 0;
`;

Documents.List = styled.div``;

Documents.Category = styled.div`
  display: flex;
  padding-bottom: 1rem;
`;

Documents.CategoryName = styled.div`
  font-weight: bold;
  font-size: 14px;
  opacity: 0.9;
  width: 300px;
  flex-shrink: 0;
  padding-right: 30px;
`;

Documents.CategoryDocuments = styled.div``;

Documents.Document = styled.div`
  padding-bottom: 0.5rem;
`;

Documents.DocumentName = styled.div`
  font-weight: bold;
  font-size: 14px;
`;

Documents.Bottom = styled.div`
  padding-top: 3rem;
`;

Documents.SelectAllText = styled.div`
  font-weight: 600;
  font-size: 11px;
  text-transform: uppercase;
  color: #576473;
`;

Documents.ArrowIcon = styled.div`
  transform: rotate(90deg);
  margin-right: 10px;
`;

Documents.DownloadButton = styled.div`
  display: flex;
  align-items: center;
`;

export default Documents;
