import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql, compose, withApollo } from 'react-apollo';
import { throttle } from 'throttle-debounce';

import { DOWNLOAD_DOCUMENTS } from 'graphql/mutation';
import { Text, Checkbox, Button, Icon } from 'elements';
import Block from './Documents.style';
import { i18n } from 'lib/constants';

@withApollo
export default class Documents extends React.PureComponent {
  link = React.createRef();

  static propTypes = {
    documents: PropTypes.array,
    lang: PropTypes.string
  };

  state = {
    selectAll: false,
    selected: []
  };

  get flatArray() {
    const { documents } = this.props;
    const result =
      documents &&
      documents.reduce((acc, cur) => {
        const items = cur.documents;
        return [...acc, ...items];
      }, []);
    return result || [];
  }

  getAllUids = e => {
    return this.flatArray.map(doc => {
      return doc.uid;
    });
  };

  onChange = uid => {
    const allUids = this.getAllUids();
    const selected = [...this.state.selected];
    const index = selected.findIndex(item => item === uid);
    if (index === -1) {
      selected.push(uid);
    } else {
      selected.splice(index, 1);
    }
    this.setState({
      selected,
      selectAll: selected.length === allUids.length
    });
  };

  onDownload = throttle(1000, true, () => {
    const { client } = this.props;
    const { selected, selectAll } = this.state;

    const uids = selected;

    client
      .mutate({
        mutation: DOWNLOAD_DOCUMENTS,
        variables: {
          uids
        }
      })
      .then(res => {
        const url = res.data.downloadDocuments;
        if (url) this.updateLinkUrl(url);
      });
  });

  updateLinkUrl = url => {
    this.setState(
      {
        download: url
      },
      e => {
        this.link.current.click();
      }
    );
  };

  onSelectAllChange = e => {
    const prevValue = this.state.selectAll;

    this.setState({
      selectAll: !prevValue,
      selected: prevValue ? [] : this.getAllUids()
    });
  };

  render() {
    const { selected, selectAll } = this.state;
    const { documents, lang } = this.props;

    return (
      <Block>
        <Block.Wrapper>
          <Block.Head>
            <Block.Title>
              <Text>{i18n('documents', lang)}</Text>
            </Block.Title>
            <div>
              <Checkbox view={'tick'} value={selectAll} onChange={this.onSelectAllChange}>
                <Block.SelectAllText>{i18n('select_all', lang)}</Block.SelectAllText>
              </Checkbox>
            </div>
          </Block.Head>
          <Block.List>
            {documents &&
              documents.map(category => {
                const { name, documents: items } = category;

                return (
                  <Block.Category>
                    <Block.CategoryName>{name}</Block.CategoryName>
                    <Block.CategoryDocuments>
                      {items &&
                        items.map(item => {
                          const { name, path, date, uid } = item;
                          const value = selectAll || selected.includes(uid);
                          return (
                            <Block.Document>
                              <Checkbox
                                view={'tick'}
                                onChange={value => this.onChange(uid)}
                                value={value}
                              >
                                <Block.DocumentName>{name}</Block.DocumentName>
                              </Checkbox>
                            </Block.Document>
                          );
                        })}
                    </Block.CategoryDocuments>
                  </Block.Category>
                );
              })}
          </Block.List>
          <Block.Bottom>
            <Button view={'common'} onClick={this.onDownload} disabled={!selected.length}>
              <Block.DownloadButton>
                <Block.ArrowIcon>
                  <Icon name={'arrow'} />
                </Block.ArrowIcon>
                Скачать
              </Block.DownloadButton>
            </Button>
          </Block.Bottom>
          <a
            download
            target="_blank"
            href={this.state.download + '?download'}
            style={{ visibility: 'hidden' }}
            ref={this.link}
          >
            скачать
          </a>
        </Block.Wrapper>
      </Block>
    );
  }
}
