const routes = require('next-routes');

module.exports = routes()
  .add({ name: 'index', pattern: '/', page: 'index' })
  .add({ name: 'resetPassword', pattern: '/reset-password', page: 'resetPassword' })
  .add({ name: 'settings', pattern: '/settings', page: 'settingsPage' })
  .add({ name: 'createPassword', pattern: '/set-password', page: 'createPassword' })
  .add({ name: 'restorePassword', pattern: '/restore-password', page: 'restorePassword' })
  .add({ name: 'createInvite', pattern: '/createInvite', page: 'createInvite' })
  .add({ name: 'detail', pattern: '/clearance/:uid', page: 'index' });
