export function i18n(_word, lang) {
  const word = _word || '';
  const key = word.toLowerCase();

  return (translation[key] && translation[key][lang]) || word;
}

export const translation = {
  customs_clearance: {
    ru: 'Таможенное оформление',
    en: 'Customs clearance'
  },
  status_and_date: {
    ru: 'Статус и дата',
    en: 'Status and date'
  },
  customs_and_coordinator: {
    ru: 'Таможня и координатор',
    en: 'Customs and coordinator'
  },
  transportation: {
    ru: 'Транспортировка',
    en: 'Transportation'
  },
  loading: {
    ru: 'Загрузка',
    en: 'Loading'
  },
  archive: {
    ru: 'Архив',
    en: 'Archive'
  },
  customs: {
    ru: 'Таможня',
    en: 'Customs'
  },
  types: {
    ru: 'Направление',
    en: 'Direction'
  },
  comings: {
    ru: 'Прибытие',
    en: 'Arrival'
  },
  coming: {
    ru: 'Прибытие',
    en: 'Arrival'
  },
  greenlights: {
    ru: 'Green Light',
    en: 'Green Light'
  },
  greenlight: {
    ru: 'Green Light',
    en: 'Green Light'
  },
  declarations: {
    ru: 'Декларация',
    en: 'Declarations'
  },
  declaration: {
    ru: 'Декларация',
    en: 'Declarations'
  },
  required_field: {
    ru: 'Обязательное поле',
    en: 'Required field'
  },
  transportations: {
    ru: 'Транспортировка',
    en: 'Transportations'
  },
  reset_filter: {
    ru: 'Сбросить фильтр',
    en: 'Clear filters'
  },
  too_simple_password: {
    ru: 'Слишком простой пароль',
    en: 'Too simple password'
  },
  download_manual: {
    ru: 'Скачать инструкцию пользователя',
    en: 'Download user manual'
  },
  online_manual: {
    ru: 'Посмотреть инструкцию пользователя онлайн',
    en: 'View user manual online'
  },
  min_length_field: {
    ru: 'Минимальное количество символов',
    en: 'Minimum number of characters'
  },
  required_condition: {
    ru: 'Обязательное условие',
    en: 'Required condition'
  },
  incorrect_email: {
    ru: 'Неверный формат email',
    en: 'Incorrect email format'
  },
  incorrect_phone: {
    ru: 'Проверьте написание телефона',
    en: 'Incorrect phone number'
  },
  client_ref: {
    ru: 'Клиент-референс',
    en: 'Enter Client-reference'
  },
  created: {
    ru: 'Создана',
    en: 'Created'
  },
  stage_title: {
    ru: 'Статусы таможенного оформления',
    en: 'Customs clearance statuses'
  },
  all_time: {
    ru: 'За все время',
    en: 'During all the time'
  },
  import: {
    ru: 'Импорт',
    en: 'Import'
  },
  export: {
    ru: 'Экспорт',
    en: 'Export'
  },
  route: {
    ru: 'Маршрут',
    en: 'Route'
  },
  setting: {
    ru: 'Настройки',
    en: 'Settings'
  },
  your_data: {
    ru: 'Ваши данные',
    en: 'Your data'
  },
  notify_by_mail: {
    ru: 'Уведомлять по почте',
    en: 'Notify by e-mail'
  },
  sign_out: {
    ru: 'Выйти из системы',
    en: 'Logout'
  },
  change_password: {
    ru: 'Сменить пароль',
    en: 'Change password'
  },
  current_password: {
    ru: 'Текущий пароль',
    en: 'Current password'
  },
  new_password: {
    ru: 'Новый пароль',
    en: 'New password'
  },
  repeat_password: {
    ru: 'Повторите пароль',
    en: 'Repeat password'
  },
  save: {
    ru: 'Сохранить',
    en: 'Save'
  },
  passwords_do_not_match: {
    ru: 'Пароли не совпадают',
    en: 'Passwords do not match'
  },
  fields_require_password: {
    ru:
      'Все поля обязательные. Пароль должен содержать минимум 8 символов, одну строчную и большую буквы, а также хотя-бы одну цифру.',
    en:
      'All fields are required. The password must contain at least 8 characters, one lowercase and capital letter, as well as at least one number.'
  },
  change_password_success: {
    ru: 'Вы успешно сменили пароль',
    en: 'Password changed successfully'
  },
  change_password_error: {
    ru: 'Вы не правильно ввели текущий пароль. Смена пароля не прошла',
    en: 'You have not entered the current password correctly. Password change failed'
  },
  password_recovery: {
    ru: 'Восстановление пароля. Придумайте новый пароль',
    en: 'Password recovery. Create a new password'
  },
  create_password: {
    ru: 'Придумайте пароль, чтобы начать отслеживать таможенное оформление грузов',
    en: 'Come up with a password to start tracking customs clearance'
  },
  check_correct_fields: {
    ru: 'Проверьте правильность заполнения полей',
    en: 'Check the correctness of the fields'
  },
  submitted: {
    ru: 'Отправлено',
    en: 'Sent'
  },
  send: {
    ru: 'Отправить',
    en: 'Send'
  },
  user_email_without: {
    ru: 'Пользователя с таким email не существует',
    en: 'There is no user with such email.'
  },
  restore_access: {
    ru: 'Восстановить доступ',
    en: 'Restore access'
  },
  email_sent: {
    ru: 'Письмо отправлено',
    en: 'Email sent'
  },
  email_sent_link: {
    ru:
      'Мы отправим письмо со ссылкой для восстановлением пароля на e-mail, указанный при регистрации в системе',
    en:
      'We will send an email with a link to reset your password to the e-mail address you provided when registering with the system'
  },
  to_email: {
    ru: 'на ваш e-mail',
    en: 'on your e-mail'
  },
  invalid_login_or_password: {
    ru: 'Неверный логин или пароль. Проверьте правильность данных.',
    en: 'Wrong login or password. Check the correctness of the data.'
  },
  request_failed: {
    ru: 'Ошибка запроса',
    en: 'The request failed'
  },
  hello: {
    ru: 'Войдите в кабинет, чтобы отслеживать таможенное оформление грузов',
    en: 'Enter the account to track customs clearance'
  },
  email: {
    ru: 'Email',
    en: 'Email'
  },
  password: {
    ru: 'Пароль',
    en: 'Password'
  },
  forget_password: {
    ru: 'Забыли пароль?',
    en: 'Forgot your password?'
  },
  login: {
    ru: 'Войти',
    en: 'Enter'
  },
  password_success_change: {
    ru: 'Новый пароль успешно изменен. Через 5 секунд вас переместит на гланвую страницу',
    en:
      'The new password is successfully changed. After 5 seconds you will be moved to the main page'
  },
  password_error: {
    ru:
      'Пароль должен содержать минимум 8 символов, одну строчную и большую буквы, а также хотя-бы одну цифру.',
    en:
      'All fields are required. The password must contain at least 8 characters, one lowercase and capital letter, as well as at least one number.'
  },
  passwords_not_match: {
    ru: 'Пароли не совпадают',
    en: 'Passwords do not match'
  },
  incorrect_link: {
    ru: 'Некорректная ссылка',
    en: 'Incorrect link'
  },
  your_login: {
    ru: 'ваш логин',
    en: 'your login'
  },
  continue: {
    ru: 'Далее',
    en: 'Next'
  },
  not_found: {
    ru: 'Страницы не существует',
    en: 'Page does not exist'
  },
  server_not_responding: {
    ru: 'Сервер не отвечает',
    en: 'Server is not responding'
  },
  error: {
    ru: 'Ошибка',
    en: 'Error'
  },
  to_home: {
    ru: 'На главную',
    en: 'On main page'
  },
  settings_saved: {
    ru: 'Настройки успешно сохранены.',
    en: 'Settings saved successfully.'
  },
  settings_error: {
    ru: 'Произошла ошибка при изменении настроек.',
    en: 'An error occurred while changing settings.'
  },
  no_requests_parameters: {
    ru: 'Нет заявок по заданным параметрам',
    en: 'No customs clearances for the specified parameters'
  },
  all_request: {
    ru: 'Все заявки',
    en: 'All customs clearance'
  },
  about_us: {
    ru: 'О нас',
    en: 'About us'
  },
  statuses: {
    ru: 'Статусы',
    en: 'Statuses'
  },
  documents: {
    ru: 'Документы',
    en: 'Documents'
  },
  empty_documents: {
    ru: 'Документов нет',
    en: 'No documents'
  },
  select_all: {
    ru: 'Выбрать все',
    en: 'Select all'
  }
};
