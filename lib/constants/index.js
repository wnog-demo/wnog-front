export const KEY = {
  ESC: 27,
  ENTER: 13,
  TAB: 9
};

export const STAGES = {
  GREEN_LIGHT: 'greenlight',
  COMING: 'coming',
  DECLARATION: 'declaration'
};

export const USER_ROLES = {
  MANAGER: 'manager'
};

export const STAGE_STATUSES = {
  SUCCESS: 'success',
  PROCESS: 'process',
  FAILURE: 'failure'
};

export const STAGE_NAMES = {
  [STAGES.GREEN_LIGHT]: 'Green light',
  [STAGES.COMING]: 'Прибытие',
  [STAGES.DECLARATION]: 'Декларация'
};

export const TRANSPORT_TYPES = {
  SHIP: 'b/l',
  TRAIN: 'rwb',
  PLANE: 'awb',
  CAR: 'cmr'
};

export const TRANSPORTATION_STATUSES = {
  SHIPPED: 'cargo-shipped',
  CONFIRMED: 'confirmed'
};

export const API_URL = '/graphql';

export const PHONE = '8122422222';

export * from './i18n.js';
