import withApollo from 'next-with-apollo';
import { ApolloClient, InMemoryCache, HttpLink, ApolloLink } from 'apollo-boost';
import { getCookie, setCookie } from 'lib/utils';
import { API_URL } from 'lib/constants';

const url = (process.env.URL || '') + API_URL;
const httpLink = new HttpLink({ uri: url });

export default withApollo(
  ({ ctx, headers, initialState }) => {
    let store = '';

    if (process.browser) {
      store = document.cookie;
    } else if (headers && headers.cookie) {
      store = headers.cookie || ' ';
    }

    const token = getCookie('auth_token', store);
    const language = (ctx && ctx.query.lang) || getCookie('language', store);

    const authLink = new ApolloLink((operation, forward) => {
      operation.setContext({
        headers: {
          authorization: token ? `Bearer ${token}` : '',
          'X-Lang': language ? language : ''
        }
      });

      return forward(operation);
    });

    return new ApolloClient({
      ssrMode: true,
      link: authLink.concat(httpLink),
      cache: new InMemoryCache().restore(initialState || {}),
      getDataFromTree: 'always'
    });
  },
  { getDataFromTree: 'ssr' }
);
