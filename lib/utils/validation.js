/*
const config = {
    type: 'phone',
    options: {
      required: true,
      maxLength: 3,
      email: true,
    },
    messages: {
      required: 'Необходимо ввести что-нибудь',
      maxLength: 'Кастомная ошибка'
    },
}
*/

import { i18n } from 'lib/constants';

// export const validate = (value, config, lang = 'ru') => {
//   const { type, options: _options = [] } = config;
//   const messages = config.messages || {};

//   const validation = {
//     required(value, arg) {
//       const message = i18n('required_field', lang);
//       const isValid = arg ? value.length >= 1 : true;

//       return { isValid, message: messages['required'] || message };
//     },
//     email(value, arg) {
//       const message = i18n('incorrect_email', lang);
//       const isValid = arg ? /(.+)@(.+){2,}\.(.+){2,}/.test(value) : true;

//       return { isValid, message: messages['email'] || message };
//     },
//     mediumPassword(value, arg) {
//       const message = 'Слишком простой пароль';
//       const isValid = arg ? /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/.test(value) : true;

//       return { isValid, message: messages['mediumPassword'] || message };
//     },
//     minLength(value, arg) {
//       const message = 'Минимальное количество символов - ' + arg;
//       const isValid = arg ? value.length >= arg : true;

//       return { isValid, message: messages['minLength'] || message };
//     },
//     checked(value, arg) {
//       const message = 'Обязательное условие';
//       const isValid = arg ? value : true;

//       return { isValid, message: messages['checked'] || message };
//     },
//     phone(value, arg) {
//       const message = 'Проверьте написание телефона';
//       const isValid = arg ? value.replace(/\D/g, '').length === 11 : true;

//       return { isValid, message: messages['phone'] || message };
//     }
//   };

//   const types = {};
//   const options = { ...types[type], ..._options };
//   let result = '';

//   Object.keys(options).some(key => {
//     const argument = options[key];
//     const validationObject = validation[key](value, argument);
//     result = validationObject;

//     return validationObject && !validationObject.isValid;
//   });
//   return result;
// };

// export const onInputChange = (fields, name, value, validateConfig, lang = 'ru') => {
//   const field = { ...fields[name] };
//   const cfg = validateConfig[name];

//   field.value = value;
//   if (cfg) {
//     const validationObject = validate(value, cfg, lang);
//     const { isValid, message } = validationObject;
//     field.isValid = isValid;
//     field.errorMessage = message;
//   }

//   return {
//     ...fields,
//     [name]: {
//       ...field
//     }
//   };
// };

// export const validateForm = (fields, cfg, lang = 'ru') => {
//   const copy = { ...fields };

//   Object.keys(cfg).forEach(name => {
//     const field = copy[name];
//     const validationObject = validate(field.value, cfg[name], lang);
//     const { isValid, message } = validationObject;
//     field.isValid = isValid;
//     field.errorMessage = message;

//     if (!field.isValid) field.showErrors = true;
//   });

//   return copy;
// };

export class Validation {
  constructor(props) {
    const { config, lang } = props;
    this.config = config || {};
    this.lang = lang || 'ru';
  }

  validate = (value, name) => {
    const { type, options: _options = [] } = this.config[name] || {};
    const lang = this.lang;
    const messages = this.config.messages || {};

    const validation = {
      required(value, arg) {
        const message = i18n('required_field', lang);
        const isValid = arg ? value.length >= 1 : true;

        return { isValid, message: messages['required'] || message };
      },
      email(value, arg) {
        const message = i18n('incorrect_email', lang);
        const isValid = arg ? /(.+)@(.+){2,}\.(.+){2,}/.test(value) : true;

        return { isValid, message: messages['email'] || message };
      },
      mediumPassword(value, arg) {
        const message = i18n('too_simple_password', lang);
        const isValid = arg ? /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/.test(value) : true;

        return { isValid, message: messages['mediumPassword'] || message };
      },
      minLength(value, arg) {
        const message = i18n('min_length_field', lang) + ' - ' + arg;
        const isValid = arg ? value.length >= arg : true;

        return { isValid, message: messages['minLength'] || message };
      },
      checked(value, arg) {
        const message = i18n('required_condition', lang);
        const isValid = arg ? value : true;

        return { isValid, message: messages['checked'] || message };
      },
      phone(value, arg) {
        const message = i18n('incorrect_phone', lang);
        const isValid = arg ? value.replace(/\D/g, '').length === 11 : true;

        return { isValid, message: messages['phone'] || message };
      }
    };

    const types = {};
    const options = { ...types[type], ..._options };
    let result = '';

    Object.keys(options).some(key => {
      const argument = options[key];
      const validationObject = validation[key](value, argument);
      result = validationObject;

      return validationObject && !validationObject.isValid;
    });

    return result;
  };

  validateForm = fields => {
    const copy = { ...fields };

    Object.keys(this.config).forEach(name => {
      const field = copy[name];
      const validationObject = this.validate(field.value, name);
      const { isValid, message } = validationObject;
      field.isValid = isValid;
      field.errorMessage = message;

      if (!field.isValid) field.showErrors = true;
    });

    return copy;
  };

  onInputChange = (fields, name, value) => {
    const field = { ...fields[name] };
    const cfg = this.config[name];

    field.value = value;
    if (cfg) {
      const validationObject = this.validate(value, name);
      const { isValid, message } = validationObject;
      field.isValid = isValid;
      field.errorMessage = message;
    }

    return {
      ...fields,
      [name]: {
        ...field
      }
    };
  };
}

export const deepCloneObject = obj => {
  return JSON.parse(JSON.stringify(obj));
};
