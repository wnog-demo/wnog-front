export const deepCloneObject = obj => {
  return JSON.parse(JSON.stringify(obj));
};

export const setCookie = (name, value, days, _store) => {
  const store = _store ? _store : process.browser ? document : {};
  let expires = '';
  if (days) {
    let date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = '; expires=' + date.toUTCString();
  }
  store.cookie = name + '=' + (value || '') + expires + '; path=/';
};

export const getCookie = (name, _store) => {
  let nameEQ = name + '=';
  const store = _store ? _store : process.browser ? document.cookie : '';
  const ca = store.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
};

export const eraseCookie = name => {
  setCookie(name, '', -1);
};

export const signIn = token => {
  setCookie('auth_token', token);
  location.replace('/');
};

export const logout = e => {
  eraseCookie('auth_token');
  location.reload();
};

export const formatTime = time => {
  return time * 1000;
};

export const throwErrorPage = error => {
  const e = new Error(error || 'Response not found');
  e.code = 'ENOENT';
  throw e;
};

export const getLast = array => {
  return array && Array.isArray(array) && !!array.length && array[array.length - 1];
};

export const declOfNum = (number, titles) => {
  const cases = [2, 0, 1, 1, 1, 2];
  return titles[
    number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5]
  ];
};

export const getScrollPosition = e => {
  return window.pageYOffset !== undefined
    ? window.pageYOffset
    : (document.documentElement || document.body.parentNode || document.body).scrollTop;
};

export const urlSearch = {
  get: name => {
    let result = null,
      tmp = [];
    location.search
      .substr(1)
      .split('&')
      .forEach(function(item) {
        tmp = item.split('=');
        if (tmp[0] === name) result = decodeURIComponent(tmp[1]);
      });
    return result;
  },
  remove: (key, sourceURL) => {
    let rtn = sourceURL.split('?')[0],
      param,
      params_arr = [],
      queryString = sourceURL.indexOf('?') !== -1 ? sourceURL.split('?')[1] : '';
    if (queryString !== '') {
      params_arr = queryString.split('&');
      for (let i = params_arr.length - 1; i >= 0; i -= 1) {
        param = params_arr[i].split('=')[0];
        if (param === key) {
          params_arr.splice(i, 1);
        }
      }
      rtn = rtn + (params_arr.length !== 0 ? '?' + params_arr.join('&') : '');
    }
    return rtn;
  }
};
