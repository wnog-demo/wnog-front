import tools from './tools';
import { css, createGlobalStyle } from 'styled-components';

export default {
  tools,
  color: {
    darkBlue: '#2D569B',
    blue: '#2cabfc',
    lightblue: '#eaf7ff',
    black: '#252841',
    white: '#ffffff',
    lightgray: '#e8e9ec',
    bgGray: '#f9fafb',
    sidebarBgGray: '#f3f5f7',
    darkgray: '#3a3d54',
    red: '#ff1901',
    yellow: '#ffb906',
    green: '#12dfa8'
  },
  overflow: createGlobalStyle`
    body {
      top: 0;
      left: 0;
      overflow: hidden;
      width: 100%;
      height: 100vh;
    }
  `
};
