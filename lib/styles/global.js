import { css } from 'styled-components';
import provider from './provider';

export default css`
  html {
    font-size: 16px;
    overflow: auto;
    overflow-y: scroll;
  }
  body {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    font-family: Graphik;
    color: ${provider.color.black};
    background-color: ${provider.color.bgGray};
    line-height: 1.5;
    word-break: break-word;
    word-wrap: break-word;
    font-size: 16px;
  }
  * {
    box-sizing: border-box;
  }
`;
