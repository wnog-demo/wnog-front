import { css } from 'styled-components';

const tools = {};

tools.fit = `
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
`;

export default tools;
