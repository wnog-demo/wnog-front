import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducers from './reducers';
import { types as TYPE } from './actions';
import { createLogger } from 'redux-logger';
import initial from './initialState';

const loggerIgnoreList = [
  TYPE.SET_TITLE,
];

const logger = createLogger({
  predicate: (getState, action) => !loggerIgnoreList.includes(action.type)
});

const middleware = [thunk, process.browser && logger];

export const initStore = (initialState = initial) => {
  return createStore(reducers, initialState, composeWithDevTools(applyMiddleware(...middleware.filter(item => !!item))));
};
