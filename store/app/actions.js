import ACTIONS from './types';

const actions = {};

actions.changeLanguage = lang => ({
  type: ACTIONS.CHANGE_LANGUAGE,
  lang
});

actions.changeDimensions = (width, height) => ({
  type: ACTIONS.CHANGE_DIMENSIONS,
  width,
  height
});

export default actions;
