import ACTIONS from './types';
import initialState from '../initialState';
import { setCookie } from 'lib/utils';

const reducers = {};

reducers[ACTIONS.CHANGE_LANGUAGE] = (state, { lang }) => {
  setCookie('language', lang);
  return {
    ...state,
    lang
  };
};

reducers[ACTIONS.CHANGE_DIMENSIONS] = (state, { width, height }) => {
  return {
    ...state,
    width,
    height
  };
};

export default function(state = initialState.app, action) {
  return reducers[action.type] ? reducers[action.type](state, action) : state;
}
