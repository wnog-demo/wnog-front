import ACTIONS from './types';

const actions = {};

actions.changeToken = token => ({
  type: ACTIONS.CHANGE_TOKEN,
  token
});

export default actions;
