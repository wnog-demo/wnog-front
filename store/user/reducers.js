import ACTIONS from './types';
import initialState from '../initialState';
import { setCookie } from 'lib/utils';

const reducers = {};

reducers[ACTIONS.CHANGE_TOKEN] = (state, { token }) => {
  setCookie('auth_token', token);
  return {
    ...state,
    token
  };
};

export default function(state = initialState.user, action) {
  return reducers[action.type] ? reducers[action.type](state, action) : state;
}
