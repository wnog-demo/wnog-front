export default {
  app: {
    lang: 'ru',
    width: null,
    height: null
  },
  notification: {
    html: '',
    type: null,
    active: false
  },
  user: {
    token: null
  },
  filter: {
    options: {},
    query: '',
    clearanceOffset: 0,
    lastScroll: 0
  }
};
