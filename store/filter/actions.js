import ACTIONS from './types';

const actions = {};

actions.changeFilterOption = (name, value, subname) => ({
  type: ACTIONS.CHANGE_FILTER_OPTION,
  subname,
  name,
  value
});

actions.resetFilter = e => ({
  type: ACTIONS.RESET_FILTER
});

actions.changeSearchQuery = query => ({
  type: ACTIONS.CHANGE_SEARCH_QUERY,
  query
});

actions.changeClearanceOffset = clearanceOffset => ({
  type: ACTIONS.CHANGE_CLEARANCE_OFFSET,
  clearanceOffset
});

actions.updateLastScroll = lastScroll => ({
  type: ACTIONS.UPDATE_LAST_SCROLL,
  lastScroll
});

export default actions;
