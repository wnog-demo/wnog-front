import ACTIONS from './types';
import initialState from '../initialState';

const reducers = {};

reducers[ACTIONS.CHANGE_FILTER_OPTION] = (state, { name, subname, value }) => {
  if (!subname) {
    return {
      ...state,
      clearanceOffset: 0,
      options: {
        ...state.options,
        [name]: Array.isArray(value) ? [...new Set(value)] : value
      }
    };
  } else {
    return {
      ...state,
      clearanceOffset: 0,
      options: {
        ...state.options,
        [subname]: value
      }
    };
  }
};

reducers[ACTIONS.RESET_FILTER] = state => {
  return {
    ...state,
    options: {},
    query: '',
    clearanceOffset: 0
  };
};

reducers[ACTIONS.CHANGE_SEARCH_QUERY] = (state, { query }) => {
  return {
    ...state,
    query,
    clearanceOffset: 0
  };
};

reducers[ACTIONS.CHANGE_CLEARANCE_OFFSET] = (state, { clearanceOffset }) => {
  return {
    ...state,
    clearanceOffset
  };
};

reducers[ACTIONS.UPDATE_LAST_SCROLL] = (state, { lastScroll }) => {
  return {
    ...state,
    lastScroll
  };
};

export default function(state = initialState.filter, action) {
  return reducers[action.type] ? reducers[action.type](state, action) : state;
}
