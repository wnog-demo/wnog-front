import ACTIONS from './types';
import initialState from '../initialState';
import { setCookie } from 'lib/utils';

const reducers = {};

reducers[ACTIONS.SHOW_NOTIFICATION] = (state, { html, _type }) => {
  return {
    ...state,
    active: true,
    type: _type || null,
    html: html || ''
  };
};

reducers[ACTIONS.CHANGE_NOTIFICATION_STATE] = (state, { active }) => {
  return {
    ...state,
    active
  };
};

export default function(state = initialState.notification, action) {
  return reducers[action.type] ? reducers[action.type](state, action) : state;
}
