import ACTIONS from './types';

const actions = {};

actions.showNotification = (html, _type) => ({
  type: ACTIONS.SHOW_NOTIFICATION,
  html,
  _type
});

actions.changeNotificationState = active => ({
  type: ACTIONS.CHANGE_NOTIFICATION_STATE,
  active
});

export default actions;
