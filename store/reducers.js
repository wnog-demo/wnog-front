import { combineReducers } from 'redux';

import appReducers from './app/reducers';
import userReducers from './user/reducers';
import notificationReducers from './notification/reducers';
import filterReducers from './filter/reducers';

export default combineReducers({
  app: appReducers,
  user: userReducers,
  notification: notificationReducers,
  filter: filterReducers
});
