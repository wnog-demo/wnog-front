import appActions from './app/actions';
import userActions from './user/actions';
import notificationActions from './notification/actions';
import filterActions from './filter/actions';

import appTypes from './app/types';

export default {
  ...appActions,
  ...userActions,
  ...notificationActions,
  ...filterActions
};

export const types = {
  ...appTypes
};
